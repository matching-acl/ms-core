FROM python:3.8.2 as builder

RUN mkdir /install
WORKDIR /install
COPY requirements.txt requirements.txt
RUN pip install --trusted-host pypi.python.org --trusted-host files.pythonhosted.org --trusted-host pypi.org --prefix /install -r requirements.txt

WORKDIR /app
COPY  . .

FROM python:3.8.2-slim
RUN apt-get update -y && apt-get install -y libpq-dev
COPY --from=builder /install /usr/local
COPY --from=builder /app /opt/aclconcilia/
ENV PYTHONPATH=/opt/aclconcilia/