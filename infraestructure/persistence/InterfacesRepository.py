import json
import numbers
import traceback

import requests
from apscheduler.triggers.cron import CronTrigger

from interface import implements
from requests.auth import HTTPBasicAuth
from core.abstractions.postgres.IInterfacesRepository import IInterfacesRepository
from core.configuration.PostgresConnection import PostgresConnection
from infraestructure.resources.api_rest_urls import ApiRestURLs
from infraestructure.resources.message_response import MessageResponse
from infraestructure.resources.model_resource import ModelResource
from infraestructure.resources.params_resource import ParamsResource
from infraestructure.resources.status_response import StatusResponse
from service.postgres import help_func


class InterfacesRepository(implements(IInterfacesRepository)):

    def __init__(self):
        self.__connection = None
        self.message_class = MessageResponse()
        self.status_class = StatusResponse()
        self.models = ModelResource()
        self.params = ParamsResource()
        self.api_rest = ApiRestURLs()

    def make_connection(self):
        self.__connection = PostgresConnection()
        return self.__connection

    def close_connection(self):
        self.__connection.close()

    def put_object(self, request, i_postgres_repository, object_cron):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.id_name, self.params.object]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                id_name, object = parameters[self.params.id_name], parameters[self.params.object]
                if schema == "" or model == "" or id_name == "" or not object or object == "":
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                object_new = object.copy()
                if not isinstance(object, dict):
                    object_new = json.loads(object)
                if model == self.models.model_interfaces:
                    name = help_func.convert_valid_names(object_new[self.params.name])
                    unique = {self.params.name: name}
                    if i_postgres_repository.find_object(schema=schema, model=model, filters=unique):
                        result = self.message_class.message_exists_object
                        status = self.status_class.status_error
                        return i_postgres_repository.result_to_json(status, result)
                    code = i_postgres_repository.available_code(object_new[self.params.name], schema, model)
                    object_new[self.params.code] = code
                    object_new[self.params.name] = name
                    object_new[self.params.cron_time_text] = i_postgres_repository.cron_description(
                        object_new[self.params.cron_time]
                    )
                    conn = self.make_connection()
                    conn.set_cursor_dictionary()
                    cursor = conn.cursor
                    try:
                        query = 'SELECT MAX(' + schema + '.' + model + '.' + id_name + ') FROM ' + schema + '.' + model
                        cursor.execute(query)
                        actual_id = cursor.fetchone()[0]
                        object_new[id_name] = '1'
                        if actual_id:
                            object_new[id_name] = str(actual_id + 1)
                        # if schema == 'acl_koncilia' and model == 'interfaces':
                        #     object_new['cron_time'] = i_postgres_repository.my_list_converter(object_new['cron_time'])
                        #     object_new['cron_time'] = '[' + help_func.replacer(object_new['cron_time'], ["'", 'True'],
                        #                                                        ['"', 'true']) + ']'
                        fields = ', '.join(object_new.keys())
                        values = ', '.join(['%%(%s)s' % x for x in object_new])
                        query = 'INSERT INTO ' + schema + '.' + model + ' (%s) VALUES (%s)' % (fields, values)
                        result = i_postgres_repository.execute(conn, cursor, query, object_new)
                        status = self.status_class.status_OK

                        conn.close()
                        self.close_connection()

                        # INICIO DE EJECUCION DEL JOB

                        object_new[self.params.schema] = schema
                        job_id = schema + '_' + object_new[self.params.code]
                        print('JOB ID: ' + job_id)
                        print('CODIGO ARCHIVOS COPIADOS: in_' + object_new[self.params.code])
                        cron_time = object_cron.evaluate_cron_expresion(object_new[self.params.cron_time])
                        print('EJECUCION PROCESAMIENTO: ' + object_new[self.params.cron_time_text])

                        object_cron.scheduler.add_job(
                            lambda: self.process_job_interfaces(
                                parameters, object_new, i_postgres_repository
                            ), CronTrigger.from_crontab(
                                cron_time
                            ), id=job_id
                        )
                        # print(object_cron.scheduler.get_jobs())
                        # if len(object_cron.scheduler.get_jobs()) == 1:
                        #     object_cron.scheduler.start()

                    except Exception as e:
                        print(e)
                        result = e
                        status = self.status_class.status_error
                        conn.close()
                        self.close_connection()
                else:
                    extra_message = self.message_class.message_extra_schema_model
                    result = self.message_class.message_invalids_params + extra_message
                    status = self.status_class.status_error
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result, status = "ACCESO DENEGADO", "400"
        return i_postgres_repository.result_to_json(status, result)

    def change_object(self, request, i_postgres_repository, object_cron):
        is_editable_active = False
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.id_name, self.params.object_new]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                id_name, object_new = parameters[self.params.id_name], parameters[self.params.object_new]
                if schema == "" or model == "" or id_name == "" or not object_new:
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                object = object_new.copy()
                filters = object_new.copy()
                if not isinstance(object_new, dict):
                    object = json.loads(object_new)
                    filters = json.loads(object_new)
                del filters[self.params.id]
                if model == self.models.model_interfaces:
                    if self.params.name in object:
                        # object[self.params.code] = help_func.coder(object[self.params.name])
                        name = help_func.convert_valid_names(object[self.params.name])
                        unique = {self.params.name: name}
                        if i_postgres_repository.find_object(schema=schema, model=model, filters=unique):
                            value = json.loads(
                                i_postgres_repository.find_object(schema=schema, model=model, filters=unique))
                            if int(value[id_name]) != int(object_new[self.params.id]):
                                result = self.message_class.message_exists_object
                                status = self.status_class.status_error
                                return i_postgres_repository.result_to_json(status, result)
                        object[self.params.name] = name
                    if self.params.cron_time in object:
                        object[self.params.cron_time_text] = i_postgres_repository.cron_description(
                            object[self.params.cron_time]
                        )
                    if self.params.active in object:
                        if object[self.params.active]:
                            template = self.validate_activation(parameters, i_postgres_repository)
                            template_active = template[self.params.active]
                            if not template_active:
                                result = self.message_class.message_cannot_activate_model.format(
                                    self.models.model_interfaces.upper(),
                                    self.models.model_templates.upper()
                                )
                                status = self.status_class.status_error
                                return i_postgres_repository.result_to_json(status, result)
                            is_editable_active = True
                        else:
                            if self.validate_inactivation(parameters, i_postgres_repository):
                                result = self.message_class.message_cannot_desactivate_model_in_use.format(
                                    self.models.model_interfaces.upper(),
                                    self.models.model_conf_conciliation.upper(),
                                    self.models.model_interfaces.upper()
                                )
                                status = self.status_class.status_error
                                return i_postgres_repository.result_to_json(status, result)
                            else:
                                unique = {self.params.id: object[self.params.id]}
                                element = json.loads(
                                    i_postgres_repository.find_object(schema=schema, model=model, filters=unique)
                                )
                                job_id = schema + '_' + element[self.params.code]
                                if object_cron.scheduler.get_job(job_id):
                                    object_cron.scheduler.remove_job(job_id)
                    conn = self.make_connection()
                    conn.set_cursor_dictionary()
                    cursor = conn.cursor
                    try:
                        id = object[self.params.id]
                        del object[self.params.id]
                        # RESTRUCTURACION DEL JOB

                        unique = {self.params.id: id}
                        element = json.loads(
                            i_postgres_repository.find_object(schema=schema, model=model, filters=unique)
                        )

                        job_id = schema + '_' + element[self.params.code]
                        cron_time = object_cron.evaluate_cron_expresion(object[self.params.cron_time])
                        element[self.params.schema] = schema
                        if self.params.template_id not in parameters[self.params.object_new]:
                            parameters[self.params.object_new][self.params.template_id] = \
                                element[self.params.template_id]

                        # if len(object_cron.scheduler.get_jobs()) >= 1:
                        if (job_id in [i.id for i in object_cron.scheduler.get_jobs()] \
                            and not self.params.active in object) or is_editable_active or \
                                job_id not in [i.id for i in object_cron.scheduler.get_jobs()]:
                            if object_cron.scheduler.get_job(job_id):
                                object_cron.scheduler.remove_job(job_id)
                            print('JOB ID (INTERFACE EDITADA): ' + job_id)
                            print('CODIGO ARCHIVOS COPIADOS: in_' + element[self.params.code])
                            print('EJECUCION PROCESAMIENTO: ' + element[self.params.cron_time_text])
                            object_cron.scheduler.add_job(
                                lambda: self.process_job_interfaces(
                                    parameters, element, i_postgres_repository
                                ), CronTrigger.from_crontab(
                                    cron_time
                                ), id=job_id
                            )
                            # if len(object_cron.scheduler.get_jobs()) == 1:
                            #     object_cron.scheduler.start()

                        query = ""

                        for keys, values in zip(object.keys(),
                                                ['%%(%s)s' % i_postgres_repository.my_list_converter(x) for x in
                                                 object]):
                            query = f"{query}, {keys} = {values}" if query else f"{keys} = {values}"
                        query = 'UPDATE ' + schema + '.' + model + ' SET ' + query + ' WHERE ' + schema + '.' + model + '.' + id_name + ' = (%s)' % (
                            id)
                        result = i_postgres_repository.execute(conn, cursor, query, object)
                        status = self.status_class.status_OK
                        conn.close()
                        self.close_connection()

                    except Exception as e:
                        print(e)
                        result = e
                        status = self.status_class.status_error
                        conn.close()
                        self.close_connection()
                else:
                    extra_message = self.message_class.message_extra_schema_model
                    result = self.message_class.message_invalids_params + extra_message
                    status = self.status_class.status_error
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result, status = "ACCESO DENEGADO", "400"
        return i_postgres_repository.result_to_json(status, result)

    def list_objects(self, request, i_postgres_repository):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                attrs = parameters[self.params.attrs] if self.params.attrs in parameters else None
                orders = parameters[self.params.orders] if self.params.orders in parameters \
                                                        else {self.params.updated : self.params.DESC}
                filters = parameters[self.params.filters] if self.params.filters in parameters else None
                if filters is not None:
                    if self.params.active in filters:
                        filters[schema + '.' + self.models.model_interfaces + '.' + self.params.active] = \
                            filters[self.params.active]
                        del filters[self.params.active]
                if schema == "" or model == "":
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                query_selectors = self.query_selectors_read(schema)
                query_orders = '' if not orders else ' ORDER BY ' + ', '.join(
                    '{} {}'.format(key, value) for key, value in orders.items())
                query_filters = '' if not filters else ' WHERE ' + ' AND '.join(
                    ['' + key + ' = %s' for key in filters.keys()])
                query_join = self.query_joins_read(schema)
                query = 'SELECT ' + query_selectors + ' FROM ' + schema + '.' + model + query_join + query_filters + query_orders
                conn = self.make_connection()
                conn.set_cursor_dictionary()
                cursor = conn.cursor
                try:
                    filter_values = None if not filters else [value for value in filters.values()]
                    cursor.execute(query, filter_values)
                    results = [i for i in cursor.fetchall()]
                    # print(results)
                    fields_names = [i[0] for i in cursor.description] if not attrs else attrs
                    data = []
                    for dato in results:
                        value = dict(zip(fields_names, dato))
                        value = {k: ('' if v is None else v) for k, v in value.items()}
                        data.append(value)
                    # total_items = len(data)
                    list_extra_parameters = [self.params.size_page, self.params.number_page]
                    if all(item in parameters_names for item in list_extra_parameters):
                        number_page = parameters[self.params.number_page]
                        size_page = parameters[self.params.size_page]
                        data = data[(number_page * size_page) - size_page: number_page * size_page]
                    data = json.dumps(
                        data,
                        ensure_ascii=False,
                        default=i_postgres_repository.my_standard_converter
                    ).encode(self.params.utf8).decode()
                    conn.close()
                    self.close_connection()
                    data = str(data).replace("\\" + '"', '"')\
                        .replace('"[', '[').replace(']"', ']')
                    # response = "{}{}{}{}: {}, {}{}{}: {} {}".format(
                    #     '{', '"', self.params.total_items, '"', str(total_items), '"', self.params.data, '"', data, '}'
                    # )
                    return data
                except Exception as e:
                    print(e)
                    result = e
                    status = self.status_class.status_error
                    conn.close()
                    self.close_connection()
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result, status = 'ACCESO DENEGADO', '400'
        return i_postgres_repository.result_to_json(status, result)

    def get_object(self, request, i_postgres_repository):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.id_name, self.params.id]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                id_name, id = parameters[self.params.id_name], str(parameters[self.params.id])
                attrs = parameters[self.params.attrs] if self.params.attrs in parameters else None
                if schema == "" or model == "" or id_name == "" or not id:
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                query_selectors = self.query_selectors_read(schema)
                query_join = self.query_joins_read(schema)
                query = 'SELECT ' + query_selectors + ' FROM ' + schema + '.' + model + query_join + ' WHERE ' + schema + '.' + model + '.' + id_name + ' = ' + id
                conn = self.make_connection()
                conn.set_cursor_dictionary()
                cursor = conn.cursor
                try:
                    cursor.execute(query)
                    result = list(cursor.fetchone())
                    fields_names = [i[0] for i in cursor.description] if not attrs else attrs
                    data = []
                    value = dict(zip(fields_names, result))
                    value = {k: ('' if v is None else v) for k, v in value.items()}
                    data.append(value)
                    data = json.dumps(data[0], ensure_ascii=False,
                                      default=i_postgres_repository.my_standard_converter).encode(
                        'utf8').decode()
                    conn.close()
                    self.close_connection()
                    return str(data).replace("\\" + '"', '"').replace('"[', '[').replace(']"', ']')
                except Exception as e:
                    print(e)
                    result = e
                    status = self.status_class.status_error
                    conn.close()
                    self.close_connection()
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result = self.message_class.message_denied_access
        #     status = self.status_class.status_error
        return i_postgres_repository.result_to_json(status, result)

    def query_selectors_read(self, schema):
        query_selectors = schema + '.interfaces.id as id, '
        query_selectors += schema + '.interfaces.name as name, '
        query_selectors += schema + '.interfaces.code as code, '
        query_selectors += schema + '.interfaces.template_id as template_id, '
        query_selectors += schema + '.templates.name as template_name, '
        query_selectors += schema + '.templates.code as template_code, '
        query_selectors += schema + '.templates.header as template_header, '
        query_selectors += schema + '.interfaces.e_origin as e_origin, '
        query_selectors += schema + '.interfaces.url as e_url, '
        query_selectors += schema + '.interfaces.entry_dir as e_entry_dir, '
        query_selectors += schema + '.interfaces.username as e_username, '
        query_selectors += schema + '.interfaces.password as e_password, '
        query_selectors += schema + '.interfaces.port as e_port, '
        query_selectors += schema + '.interfaces.cron_time as cron_time, '
        query_selectors += schema + '.interfaces.cron_time_text as cron_time_text, '
        query_selectors += schema + '.interfaces.created as created, '
        query_selectors += schema + '.interfaces.updated as updated, '
        query_selectors += schema + '.interfaces.active as active, '
        query_selectors += schema + '.interfaces.user_id as user_id, '
        query_selectors += schema + '.interfaces.is_titled as is_titled, '
        query_selectors += 'private_login.users.first_name as first_name, '
        query_selectors += 'private_login.users.last_name as last_name, '
        query_selectors += 'private_login.users.username as username '
        # query_selectors += schema + '.users.first_name as first_name, '
        # query_selectors += schema + '.users.last_name as last_name, '
        # query_selectors += schema + '.users.username as username '
        return query_selectors

    def query_joins_read(self, schema):
        query_join = ' LEFT JOIN ' + schema + '.templates ON ' + schema + '.interfaces.template_id = ' + schema + '.templates.id '
        query_join += ' LEFT JOIN ' + 'private_login.users_relations ON ' + schema + '.interfaces.user_id = private_login.users_relations.id '
        query_join += ' LEFT JOIN ' + 'private_login.users ON private_login.users_relations.id_user = private_login.users.id '
        # query_join += ' LEFT JOIN ' + schema + '.users ON ' + schema + '.interfaces.user_id = ' + schema + '.users.id '
        return query_join

    def validate_activation(self, parameters, i_postgres_repository):
        schema = parameters[self.params.schema]
        interface_id = parameters[self.params.object_new][self.params.id]
        unique = {self.params.id: interface_id}
        interface = json.loads(i_postgres_repository.find_object(
            schema=parameters[self.params.schema], model=self.models.model_interfaces, filters=unique)
        )
        unique = {self.params.id: interface[self.params.template_id]}
        return json.loads(
            i_postgres_repository.find_object(schema=schema, model=self.models.model_templates, filters=unique)
        )

    def validate_inactivation(self, parameters, i_postgres_repository):
        interface_id = parameters[self.params.object_new][self.params.id]
        unique = {self.params.id: interface_id}
        interface = json.loads(
            i_postgres_repository.find_object(
                schema=parameters[self.params.schema], model=self.models.model_interfaces, filters=unique
            )
        )
        id = interface[self.params.id] if isinstance(interface[self.params.id], numbers.Number) \
            else int(interface[self.params.id])
        unique = {self.params.active: True}
        conciliations = i_postgres_repository.find_all(
            schema=parameters[self.params.schema], model=self.models.model_conf_conciliation, filters=unique
        ).replace('"[', '[').replace(']"', ']')
        interfaces = [x for o in json.loads(conciliations) for x in o[self.params.interfaces]]
        interfaces = [dict(t) for t in {tuple(d.items()) for d in interfaces}]
        interfacesIds = list(map(
            lambda x: x[self.params.interfaceId]
            if isinstance(x[self.params.interfaceId], numbers.Number)
            else int(x[self.params.interfaceId]),
            interfaces
        ))
        return id in interfacesIds

    def rollback_error(self):
        conn = self.make_connection()
        conn.set_cursor_dictionary()
        cursor = conn.cursor
        cursor.execute(self.message_class.message_execute_rollback)

    def get_template(self, parameters, object, i_postgres_repository):
        unique = {self.params.id: object[self.params.template_id]}
        tupla = i_postgres_repository.find_object(
                schema=parameters[self.params.schema], model=self.models.model_templates, filters=unique
            )
        print(tupla)
        template = json.loads(tupla)
        return template

    def request_for_load_data(self, parameters, object, i_postgres_repository):
        template = self.get_template(parameters, object, i_postgres_repository)
        return json.dumps({
            self.params.schema: parameters[self.params.schema],
            self.params.model: parameters[self.params.model],
            self.params.code: object[self.params.code],
            self.params.header: template[self.params.header],
            self.params.separator: template[self.params.separator]
        })

    def copy_files_job_interfaces(self, object):
        job_id = object[self.params.schema] + '_' + object[self.params.code]
        print('JOB ID: ' + job_id)
        entry_dir = self.params.entry_dir \
            if self.params.entry_dir in object \
            else help_func.underscore_to_camelcase(self.params.entry_dir)
        object[self.params.local_address] = object[entry_dir]
        object[self.params.user_name] = object[self.params.username]
        object_dict = {}
        for k, v in object.items():
            if '_' in k:
                object_dict[help_func.underscore_to_camelcase(k)] = v
            else:
                object_dict[k] = v
        object = object_dict
        # self.object_job = {
        #     help_func.underscore_to_camelcase(k): v for k, v in self.object_job.items() if '_' in k
        # }
        headers = {'Content-type': self.params.content_type, 'Accept': self.params.content_type}
        try:
            result = requests.post(
                self.api_rest.prod_files_host + self.api_rest.route_dir_create,
                data=json.dumps(object),
                headers=headers,
                auth=HTTPBasicAuth(self.params.user_prod_files_host, self.params.password_prod_files_host))
            result = {self.params.status: result.status_code, self.params.message: result.json()[self.params.message]}
        except:
            tb = 'ERROR COPYING: ' + traceback.format_exc()
        else:
            tb = 'COPYING FILES:' + str(result)
        finally:
            print(tb)

    def process_job_interfaces(self, parameters, object, i_postgres_repository):
        # print('HOLA MUNDO...')
        self.copy_files_job_interfaces(object)
        # data = self.request_for_load_data(parameters, object, i_postgres_repository)
        # print(data)
        headers = {'Content-type': self.params.content_type, 'Accept': self.params.content_type}
        try:
            result = requests.post(
                self.api_rest.prod_load_data_host + self.api_rest.route_load_data,
                # self.api_rest.dev_load_data_host + self.api_rest.route_load_data,
                data=self.request_for_load_data(parameters, object, i_postgres_repository),
                headers=headers
            )
        except:
            tb = 'ERROR PROCESSING: ' + traceback.format_exc()
        else:
            tb = 'PROCESSING FILES: ' + str(result.json())
        finally:
            print(tb)
