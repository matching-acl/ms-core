import json
import string

import inject
import requests
import datetime

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
from interface import implements
from core.abstractions.postgres.ICronRepository import ICronRepository
from infraestructure.resources.model_resource import ModelResource
from infraestructure.resources.params_resource import ParamsResource
from infraestructure.resources.api_rest_urls import ApiRestURLs
from croniter import croniter
from datetime import datetime, timedelta
import time

class CronRepository(implements(ICronRepository)):

    def __init__(self):
        self.params = ParamsResource()
        self.models = ModelResource()
        # self.sched = BlockingScheduler()
        self.sched = BackgroundScheduler()
        self.api_rest = ApiRestURLs()
        # self.__inject_conf_conciliation = inject.clear_and_configure(
        #     lambda binder: binder.bind(IConfConciliationRepository, ConfConciliationRepository())
        # )
        # self.object_con_conciliation = self.__inject_conf_conciliation.get_instance(IConfConciliationRepository)
        # self.counter = 0

    @property
    def scheduler(self):
        return self.sched

    def add_job(self, model, object):
        cron_time = object[self.params.cron_time]
        if model == self.models.model_interfaces:
            self.sched.add_job(self.process_job_interfaces, CronTrigger.from_crontab(cron_time), id=model)

    def execute_cron(self, model, object):
        self.add_job(model, object)
        self.sched.start()

    def stop_cron(self, id_job):
        self.sched.remove_job(id_job)
        print('JOB ' + id_job + 'DETENIDO CORRECTAMENTE')

    def round_down_time(self, dt=None, dateDelta=timedelta(minutes=1)):
        roundTo = dateDelta.total_seconds()
        if dt == None: dt = datetime.now()
        seconds = (dt - dt.min).seconds
        rounding = (seconds + roundTo / 2) // roundTo * roundTo
        return dt + timedelta(0, rounding - seconds, -dt.microsecond)

    def get_next_cron_run_time(self, schedule):
        return croniter(schedule, datetime.now()).get_next(datetime)

    def sleep_till_top_of_next_minute(self):
        t = datetime.utcnow()
        sleeptime = 60 - (t.second + t.microsecond / 1000000.0)
        time.sleep(sleeptime)

    def process_job_interfaces(self):
        from core.abstractions.postgres.IInterfacesRepository import IInterfacesRepository
        from infraestructure.persistence.InterfacesRepository import InterfacesRepository
        inject_interfaces = inject.clear_and_configure(
            lambda binder: binder.bind(IInterfacesRepository, InterfacesRepository())
        )
        object_interfaces = inject_interfaces.get_instance(IInterfacesRepository)
        result = requests.post(
            self.api_rest.prod_load_data_host + self.api_rest.route_load_data,
            data=json.dumps(object_interfaces.request_for_load_data())
        )
        date = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        print(date + ' -- ' + result.json())
        return result

    def evaluate_cron_expresion(self, cron_time):
        array = cron_time.strip().split()
        if len(array) > 5:
            cron_time = ''
            for i in range(1, len(array)):
                if self.cron_contains_letters(array[i]):
                    array[i] = '*'
                if i != 3:
                    cron_time += array[i] + ' '
        return cron_time

    def cron_after_minutes(self, cron_time, minutes):
        array = cron_time.strip().split()
        min_cron = array[0]
        if '/' in min_cron:
            array_min = min_cron.split('/')
            min_value = array_min[1]
            min_value = int(min_value) + minutes
            return array_min[0] + '/' + str(min_value) + ' ' + ' '.join(array[1:])
        elif '*' in min_cron and '/' not in min_cron:
            return str(minutes) + ' ' + ' '.join(array[1:])
        else:
            return str(int(min_cron) + minutes) + ' ' + ' '.join(array[1:])

    def cron_contains_letters(self, parameter):
        allowed = set(string.ascii_uppercase + string.ascii_lowercase + ',-')
        return False if set(parameter) - allowed else True


# payload = {"cron_time": "* * * * *"}
# headers = {'authorization': 'oh_so_secret oh_my_only_secret'}
# result = requests.post('http://127.0.0.1:5000/read', data=payload, headers=headers)
# cron = CronRepository()
# print(cron.cron_contains_letters('0-9'))