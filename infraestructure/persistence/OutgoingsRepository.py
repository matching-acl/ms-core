import json

from apscheduler.triggers.cron import CronTrigger
from interface import implements
from psycopg2._psycopg import IntegrityError
from psycopg2.errorcodes import UNIQUE_VIOLATION

from core.abstractions.postgres.IOutgoingsRepository import IOutgoingsRepository
from core.configuration.PostgresConnection import PostgresConnection
from infraestructure.persistence.ConfConciliationRepository import ConfConciliationRepository
from infraestructure.resources.message_response import MessageResponse
from infraestructure.resources.model_resource import ModelResource
from infraestructure.resources.params_resource import ParamsResource
from infraestructure.resources.status_response import StatusResponse
from infraestructure.resources.prefix_resource import PrefixResource
from service.postgres import help_func


class OutgoingsRepository(implements(IOutgoingsRepository)):

    def __init__(self, object_cron):
        self.__connection = None
        self.message_class = MessageResponse()
        self.status_class = StatusResponse()
        self.models = ModelResource()
        self.params = ParamsResource()
        self.prefixs = PrefixResource()
        self.__object_cron = object_cron
        self.conf_conciliation_repository = ConfConciliationRepository()

    @property
    def object_cron(self):
        return self.__object_cron

    def make_connection(self):
        self.__connection = PostgresConnection()
        return self.__connection

    def close_connection(self):
        self.__connection.close()

    def put_object(self, request, i_postgres_repository):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.object]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                object = parameters[self.params.object]
                if schema == "" or model == "" or not object or object == "":
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                object_new = object.copy()
                if not isinstance(object, dict):
                    object_new = json.loads(object)
                if model == self.models.model_outgoings:
                    # VALIDANDO QUE EL NOMBRE NO SE REPITA
                    name = help_func.convert_valid_names(object_new[self.params.name])
                    unique = {self.params.name: name}
                    if i_postgres_repository.find_object(schema=schema, model=model, filters=unique):
                        result = self.message_class.message_exists_object
                        status = self.status_class.status_error
                        return i_postgres_repository.result_to_json(status, result)
                    object_new[self.params.name] = name
                    # EFECTUANDO INSERCION
                    conn = self.make_connection()
                    conn.set_cursor_dictionary()
                    cursor = conn.cursor
                    try:
                        query = 'SELECT MAX(' + schema + '.' + model + '.' + self.params.id + ') '
                        query += 'FROM ' + schema + '.' + model
                        cursor.execute(query)
                        actual_id = cursor.fetchone()[0]
                        object_new[self.params.id] = '1'
                        if actual_id:
                            object_new[self.params.id] = str(actual_id + 1)
                        fields = ', '.join(object_new.keys())
                        values = ', '.join(['%%(%s)s' % x for x in object_new])
                        query = 'INSERT INTO ' + schema + '.' + model + ' (%s) VALUES (%s)' % (fields, values)
                        result = i_postgres_repository.execute(conn, cursor, query, object_new)
                        if isinstance(result, IntegrityError):
                            result = self.message_class.message_exists_object
                            status = self.status_class.status_error
                        else:
                            status = self.status_class.status_OK
                        conn.close()
                        self.close_connection()
                    except Exception as e:
                        print(e)
                        result = e
                        status = self.status_class.status_error
                        conn.close()
                        self.close_connection()
                else:
                    extra_message = self.message_class.message_extra_schema_model
                    result = self.message_class.message_invalids_params + extra_message
                    status = self.status_class.status_error
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result, status = "ACCESO DENEGADO", "400"
        return i_postgres_repository.result_to_json(status, result)

    def change_object(self, request, i_postgres_repository):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.object_new]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                object_new = parameters[self.params.object_new]
                if schema == "" or model == "" or not object_new:
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                object = object_new.copy()
                if not isinstance(object_new, dict):
                    object = json.loads(object_new)
                if model == self.models.model_outgoings:
                    if self.params.name in object:
                        # object[self.params.code] = help_func.coder(object[self.params.name])
                        name = help_func.convert_valid_names(object[self.params.name])
                        unique = {self.params.name: name}
                        if i_postgres_repository.find_object(schema=schema, model=model, filters=unique):
                            value = json.loads(
                                i_postgres_repository.find_object(schema=schema, model=model, filters=unique)
                            )
                            if int(value[self.params.id]) != int(object_new[self.params.id]):
                                result = self.message_class.message_exists_object
                                status = self.status_class.status_error
                                return i_postgres_repository.result_to_json(status, result)
                        object[self.params.name] = name
                    if self.params.active in object:
                        if object[self.params.active]:
                            self.validate_activation(parameters, i_postgres_repository)
                        else:
                            self.validate_inactivation(parameters, i_postgres_repository)
                    conn = self.make_connection()
                    conn.set_cursor_dictionary()
                    cursor = conn.cursor
                    try:
                        query = ""
                        id = object[self.params.id]
                        for keys, values in zip(object.keys(),
                                                ['%%(%s)s' % i_postgres_repository.my_list_converter(x) for x in object]):
                            query = f"{query}, {keys} = {values}" if query else f"{keys} = {values}"
                        query = 'UPDATE ' + schema + '.' + model + ' SET ' + query + ' WHERE ' + schema + '.' + model + '.' + self.params.id + ' = (%s)' % (
                            id)
                        result = i_postgres_repository.execute(conn, cursor, query, object)
                        if isinstance(result, IntegrityError):
                            result = self.message_class.message_exists_object
                            status = self.status_class.status_error
                        else:
                            status = self.status_class.status_OK
                        conn.close()
                        self.close_connection()
                    except Exception as e:
                        print(e)
                        result = e
                        status = self.status_class.status_error
                        conn.close()
                        self.close_connection()
                else:
                    extra_message = self.message_class.message_extra_schema_model
                    result = self.message_class.message_invalids_params + extra_message
                    status = self.status_class.status_error
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result, status = "ACCESO DENEGADO", "400"
        return i_postgres_repository.result_to_json(status, result)

    def list_objects(self, request, i_postgres_repository):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                attrs = parameters[self.params.attrs] if self.params.attrs in parameters else None
                orders = parameters[self.params.orders] if self.params.orders in parameters \
                                                        else {self.params.updated : self.params.DESC}
                filters = parameters[self.params.filters] if self.params.filters in parameters else None
                if filters is not None:
                    if self.params.active in filters:
                        filters[schema + '.' + self.models.model_outgoings + '.' + self.params.active] = \
                            filters[self.params.active]
                        del filters[self.params.active]
                if schema == "" or model == "":
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                query_selectors = self.query_selectors_read(schema)
                query_orders = '' if not orders else ' ORDER BY ' + ', '.join(
                    '{} {}'.format(key, value) for key, value in orders.items())
                query_filters = '' if not filters else ' WHERE ' + ' AND '.join(
                    ['' + key + ' = %s' for key in filters.keys()])
                query_join = self.query_joins_read(schema)
                query = 'SELECT ' + query_selectors + ' FROM ' + schema + '.' + model + query_join + query_filters + query_orders
                conn = self.make_connection()
                conn.set_cursor_dictionary()
                cursor = conn.cursor
                try:
                    filter_values = None if not filters else [value for value in filters.values()]
                    cursor.execute(query, filter_values)
                    results = [i for i in cursor.fetchall()]
                    fields_names = [i[0] for i in cursor.description] if not attrs else attrs
                    data = []
                    for dato in results:
                        value = dict(zip(fields_names, dato))
                        value = {k: ('' if v is None else v) for k, v in value.items()}
                        data.append(value)
                    # total_items = len(data)
                    list_extra_parameters = [self.params.size_page, self.params.number_page]
                    if all(item in parameters_names for item in list_extra_parameters):
                        number_page = parameters[self.params.number_page]
                        size_page = parameters[self.params.size_page]
                        data = data[(number_page * size_page) - size_page: number_page * size_page]
                    data = json.dumps(
                        data,
                        ensure_ascii=False, default=i_postgres_repository.my_standard_converter
                    ).encode(self.params.utf8).decode()
                    conn.close()
                    self.close_connection()
                    data = str(data).replace("\\" + '"', '"') \
                        .replace('"[', '[').replace(']"', ']').replace('null','')
                    # response = "{}{}{}{}: {}, {}{}{}: {} {}".format(
                    #     '{', '"', self.params.total_items, '"', str(total_items), '"', self.params.data, '"', data, '}'
                    # )
                    return data
                except Exception as e:
                    print(e)
                    result = e
                    status = self.status_class.status_error
                    conn.close()
                    self.close_connection()
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
    # else:
    #     result, status = 'ACCESO DENEGADO', '400'
        return i_postgres_repository.result_to_json(status, result)

    def get_object(self, request, i_postgres_repository):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.id]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                id = str(parameters[self.params.id])
                attrs = parameters[self.params.attrs] if self.params.attrs in parameters else None
                if schema == "" or model == "" or not id:
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                query_selectors = self.query_selectors_read(schema)
                query_join = self.query_joins_read(schema)
                query = 'SELECT ' + query_selectors + ' FROM ' + schema + '.' + model + query_join + ' WHERE ' + schema + '.' + model + '.' + self.params.id + ' = ' + id
                conn = self.make_connection()
                conn.set_cursor_dictionary()
                cursor = conn.cursor
                try:
                    cursor.execute(query)
                    result = list(cursor.fetchone())
                    fields_names = [i[0] for i in cursor.description] if not attrs else attrs
                    data = []
                    value = dict(zip(fields_names, result))
                    value = {k: ('' if v is None else v) for k, v in value.items()}
                    data.append(value)
                    data = json.dumps(data[0],
                                      ensure_ascii=False,
                                      default=i_postgres_repository.my_standard_converter
                                      ).encode(self.params.utf8).decode()
                    conn.close()
                    self.close_connection()
                    return str(data).replace("\\" + '"', '"').replace('"[', '[').replace(']"', ']').replace('null', '')
                except Exception as e:
                    print(e)
                    result = e
                    status = self.status_class.status_error
                    conn.close()
                    self.close_connection()
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result = self.message_class.message_denied_access
        #     status = self.status_class.status_error
        return i_postgres_repository.result_to_json(status, result)

    def query_selectors_read(self, schema):
        query_selectors = schema + '.outgoings.id as id, '
        query_selectors += schema + '.outgoings.name as name, '
        query_selectors += schema + '.outgoings.e_origin as e_origin, '
        query_selectors += schema + '.outgoings.url as e_url, '
        query_selectors += schema + '.outgoings.username as e_username, '
        query_selectors += schema + '.outgoings.password as e_password, '
        query_selectors += schema + '.outgoings.entry_dir as e_entry_dir, '
        query_selectors += schema + '.outgoings.port as e_port, '
        query_selectors += schema + '.outgoings.created as created, '
        query_selectors += schema + '.outgoings.updated as updated, '
        query_selectors += schema + '.outgoings.active as active, '
        query_selectors += schema + '.outgoings.user_id as user_id, '
        query_selectors += 'private_login.users.first_name as first_name, '
        query_selectors += 'private_login.users.last_name as last_name, '
        query_selectors += 'private_login.users.username as username '
        return query_selectors

    def query_joins_read(self, schema):
        query_join = ' LEFT JOIN ' + 'private_login.users_relations ON ' + schema + '.outgoings.user_id = private_login.users_relations.id '
        query_join += ' LEFT JOIN ' + 'private_login.users ON private_login.users_relations.id_user = private_login.users.id '
        # query_join += ' LEFT JOIN ' + schema + '.users ON ' + schema + '.templates.user_id = ' + schema + '.users.id '
        return query_join

    def validate_inactivation(self, parameters, i_postgres_repository):
        outgoings_id = parameters[self.params.object_new][self.params.id]
        unique = {self.params.outgoings_id: outgoings_id}
        conciliations = json.loads(i_postgres_repository.find_all(
            schema=parameters[self.params.schema], model=self.models.model_conf_conciliation, filters=unique
        ).replace('"[', '[').replace(']"', ']'))
        for conc in conciliations:
            object = {
                self.params.id: conc[self.params.id],
                self.params.out_validated: 0
            }
            i_postgres_repository.change_object(
                schema=parameters[self.params.schema],
                model=self.models.model_conf_conciliation,
                id_name=self.params.id,
                object=object
            )
            if conc[self.params.active]:
                job_id = parameters[self.params.schema] + '_' + conc[self.params.code] + self.prefixs.suffix_conciliation
                if self.object_cron.scheduler.get_job('out_' + job_id):
                    self.object_cron.scheduler.remove_job('out_' + job_id)

    def validate_activation(self, parameters, i_postgres_repository):
        outgoings_id = parameters[self.params.object_new][self.params.id]
        unique = {self.params.outgoings_id: outgoings_id}
        conciliations = json.loads(i_postgres_repository.find_all(
            schema=parameters[self.params.schema], model=self.models.model_conf_conciliation, filters=unique
        ).replace('"[', '[').replace(']"', ']'))
        for conc in conciliations:
            object = {
                self.params.id: conc[self.params.id],
                self.params.out_validated: 1
            }
            i_postgres_repository.change_object(
                schema=parameters[self.params.schema],
                model=self.models.model_conf_conciliation,
                id_name=self.params.id,
                object=object
            )
            if conc[self.params.active]:
                job_id = parameters[self.params.schema] + '_' + conc[self.params.code] + self.prefixs.suffix_conciliation
                if not self.object_cron.scheduler.get_job('out_' + job_id):
                    print('JOB ID (SALIDA CONCILIACION EDITADA): ' + 'out_' + job_id)
                    print('EJECUCION SALIDA CONCILIACION: ' + conc[self.params.cron_time_out])
                    self.object_cron.scheduler.add_job(
                        lambda: self.conf_conciliation_repository.process_job_out_conciliation(
                            parameters, conc
                        ), CronTrigger.from_crontab(
                            conc[self.params.cron_time_out]
                        ), id='out_' + job_id
                    )
                    # if len(self.object_cron.scheduler.get_jobs()) == 1:
                    #     self.object_cron.scheduler.start()


# postgres_repo = PostgresRepository()
# attrs = {'code'}
# result = json.loads(postgres_repo.find_all(schema='acl_koncilia', model='templates', attrs=attrs))
# result = list(set([x['code'] for x in result]))
# print(result)