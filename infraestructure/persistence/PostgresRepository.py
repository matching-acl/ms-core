import ast
import json
from datetime import datetime, time

from cron_descriptor import Options, CasingTypeEnum, ExpressionDescriptor, DescriptionTypeEnum
from interface import implements
from core.abstractions.postgres.IPostgresRepository import IPostgresRepository
from core.configuration.PostgresConnection import PostgresConnection
from infraestructure.resources.format_resource import FormatResource
from infraestructure.resources.message_response import MessageResponse
from infraestructure.resources.model_resource import ModelResource
from infraestructure.resources.params_resource import ParamsResource
from infraestructure.resources.status_response import StatusResponse
from service.postgres import help_func


class PostgresRepository(implements(IPostgresRepository)):

    def __init__(self):
        self.__connection = None
        self.message_class = MessageResponse()
        self.status_class = StatusResponse()
        self.models = ModelResource()
        self.params = ParamsResource()
        self.format = FormatResource()

    def make_connection(self):
        self.__connection = PostgresConnection()
        return self.__connection

    def close_connection(self):
        self.__connection.close()

    def get_object(self, request):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.id_name, self.params.object]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                id_name, id = parameters[self.params.id_name], str(parameters[self.params.id])
                attrs = parameters[self.params.attrs] if self.params.attrs in parameters else None
                if schema == "" or model == "" or id_name == "" or not id:
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return self.result_to_json(status, result)
                query_selectors = '*' if not attrs else ', '.join([i for i in attrs])
                query = 'SELECT ' + query_selectors + ' FROM ' + schema + '.' + model + ' WHERE ' + schema + '.' + model + '.' + id_name + ' = ' + id
                conn = self.make_connection()
                conn.set_cursor_dictionary()
                cursor = conn.cursor
                try:
                    cursor.execute(query)
                    result = list(cursor.fetchone())
                    fields_names = [i[0] for i in cursor.description] if not attrs else attrs
                    data = []
                    data.append(dict(zip(fields_names, result)))
                    data = json.dumps(data[0], ensure_ascii=False, default=self.my_standard_converter).encode(
                        'utf8').decode()
                    conn.close()
                    self.close_connection()
                    return str(data).replace("\\" + '"', '"')
                except Exception as e:
                    result = self.message_class.message_invalids_params_values
                    status = self.status_class.status_error
                    conn.close()
                    self.close_connection()
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result, status = 'ACCESO DENEGADO', '400'
        return self.result_to_json(status, result)

    def list_objects(self, request):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.is_enum]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema = parameters[self.params.schema]
                model = parameters[self.params.model]
                is_enum = parameters[self.params.is_enum]
                attrs = parameters[self.params.attrs] if self.params.attrs in parameters else None
                orders = parameters[self.params.orders] if self.params.orders in parameters else None
                filters = parameters[self.params.filters] if self.params.filters in parameters else None
                if schema == "" or model == "":
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return self.result_to_json(status, result)
                if is_enum == '0' or is_enum == 0:
                    query_selectors = '*' if not attrs else ', '.join([i for i in attrs])
                    query_orders = '' if not orders else ' ORDER BY ' + ', '.join(
                        '{} {}'.format(key, value) for key, value in orders.items())
                    query_filters = '' if not filters else ' WHERE ' + ' AND '.join(
                        ['' + key + ' = %s' for key in filters.keys()])
                    query = 'SELECT ' + query_selectors + ' FROM ' + schema + '.' + model + query_filters + query_orders
                else:
                    query = 'SELECT unnest(enum_range(NULL::' + schema + '.' + model + '));'
                conn = self.make_connection()
                conn.set_cursor_dictionary()
                cursor = conn.cursor
                try:
                    filter_values = None if not filters else [value for value in filters.values()]
                    cursor.execute(query, filter_values)
                    results = [i for i in cursor.fetchall()]
                    fields_names = [i[0] for i in cursor.description] if not attrs else attrs
                    if is_enum == '1':
                        fields_names = [model if x != model else x for x in fields_names]
                    data = []
                    for dato in results:
                        data.append(dict(zip(fields_names, dato)))
                    data = json.dumps(data, ensure_ascii=False, default=self.my_standard_converter).encode(
                        'utf8').decode()
                    conn.close()
                    self.close_connection()
                    return str(data).replace("\\" + '"', '"')
                except Exception as e:
                    result = self.message_class.message_invalids_params_values
                    status = self.status_class.status_error
                    conn.close()
                    self.close_connection()
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result, status = 'ACCESO DENEGADO', '400'
        return self.result_to_json(status, result)

    def find_object(self, schema="", model="", filters=None, attrs=None):
        if schema == "" or model == "" or not filters:
            return None
        query_selectors = '*' if not attrs else ', '.join([i for i in attrs])
        query_filters = '' if not filters else ' WHERE ' + ' AND '.join(['' + key + ' = %s' for key in filters.keys()])
        query = 'SELECT ' + query_selectors + ' FROM ' + schema + '.' + model + query_filters
        conn = self.make_connection()
        conn.set_cursor_dictionary()
        cursor = conn.cursor
        try:
            filter_values = None if not filters else [value for value in filters.values()]
            cursor.execute(query, filter_values)
            result = cursor.fetchone()
            if result:
                result = list(result)
                fields_names = [i[0] for i in cursor.description] if not attrs else attrs
                data = []
                data.append(dict(zip(fields_names, result)))
                data = json.dumps(
                    data[0],
                    ensure_ascii=False,
                    default=self.my_standard_converter
                ).encode(self.params.utf8).decode()
                # return str(data).replace("\\" + '"', '"')
                conn.close()
                self.close_connection()
                return data
        except Exception as e:
            conn.close()
            self.close_connection()

    def find_all(self, schema="", model="", filters=None, attrs=None):
        if schema == "" or model == "":
            result = self.message_class.message_empty_params
            status = self.status_class.status_error
            return self.result_to_json(status, result)
        query_selectors = '*' if not attrs else ', '.join([i for i in attrs])
        query_filters = '' if not filters else ' WHERE ' + ' AND '.join(['' + key + ' = %s' for key in filters.keys()])
        query = 'SELECT ' + query_selectors + ' FROM ' + schema + '.' + model + query_filters
        conn = self.make_connection()
        conn.set_cursor_dictionary()
        cursor = conn.cursor
        try:
            filter_values = None if not filters else [value for value in filters.values()]
            cursor.execute(query, filter_values)
            results = [i for i in cursor.fetchall()]
            # print(results)
            fields_names = [i[0] for i in cursor.description] if not attrs else attrs
            data = []
            for dato in results:
                data.append(dict(zip(fields_names, dato)))
            data = json.dumps(data, ensure_ascii=False, default=self.my_standard_converter).encode(
                'utf8').decode()
            conn.close()
            self.close_connection()
            return str(data).replace("\\" + '"', '"')
        except Exception as e:
            conn.close()
            self.close_connection()

    def total_items(self, request):
        parameters = help_func.parameters_to_dict(request)
        schema = parameters[self.params.schema]
        model = parameters[self.params.model]
        attrs = parameters[self.params.attrs] if self.params.attrs in parameters else None
        filters = parameters[self.params.filters] if self.params.filters in parameters else None
        if schema == "" or model == "":
            result = self.message_class.message_empty_params
            status = self.status_class.status_error
            return self.result_to_json(status, result)
        query_selectors = '*' if not attrs else ', '.join([i for i in attrs])
        query_filters = '' if not filters else ' WHERE ' + ' AND '.join(['' + key + ' = %s' for key in filters.keys()])
        query = 'SELECT ' + query_selectors + ' FROM ' + schema + '.' + model + query_filters
        conn = self.make_connection()
        conn.set_cursor_dictionary()
        cursor = conn.cursor
        try:
            filter_values = None if not filters else [value for value in filters.values()]
            cursor.execute(query, filter_values)
            results = [i for i in cursor.fetchall()]
            response = {
                self.params.total_items: len(results)
            }
            conn.close()
            self.close_connection()
            return self.result_to_extra_json(response)
        except Exception as e:
            conn.close()
            self.close_connection()

    def change_object(self, schema="", model="", id_name="", object=None):
        if schema == "" or model == "" or id_name == "" or not object:
            result = self.message_class.message_empty_params
            status = self.status_class.status_error
            return self.result_to_json(status, result)
        if not isinstance(object, dict):
            object = json.loads(object)
        conn = self.make_connection()
        conn.set_cursor_dictionary()
        cursor = conn.cursor
        try:
            query = ""
            id = object[self.params.id]
            for keys, values in zip(object.keys(),
                                    ['%%(%s)s' % self.my_list_converter(x) for x in object]):
                query = f"{query}, {keys} = {values}" if query else f"{keys} = {values}"
            query = 'UPDATE ' + schema + '.' + model + ' SET ' + query + ' WHERE ' + schema + '.' + model + '.' + id_name + ' = (%s)' % (
                id)
            result = self.execute(conn, cursor, query, object)
            status = self.status_class.status_OK
            conn.close()
            self.close_connection()
        except Exception as e:
            print(e)
            result = e
            status = self.status_class.status_error
            conn.close()
            self.close_connection()
        return self.result_to_json(status, result)

    def available_code(self, name, schema, model):
        attrs = {self.params.code}
        codes = json.loads(self.find_all(
            schema=schema, model=model, attrs=attrs)
        )
        codes = list(set([x[self.params.code] for x in codes]))
        code = help_func.coder(name)
        while code in codes:
            code = help_func.coder_random(name, self.params.max_length_code)
        return code

    def execute(self, connection, cursor, query, object):
        try:
            cursor.execute(query, object)
            connection.commit()
            return True
        except Exception as e:
            print(e)
            self.rollback_error()
            return e

    def remove_object(self, schema="", model="", id_name="", id=None):
        if schema == "" or model == "" or id_name == "" or not id:
            return None

    def rollback_error(self):
        conn = self.make_connection()
        conn.set_cursor_dictionary()
        cursor = conn.cursor
        cursor.execute(self.message_class.message_execute_rollback)
        self.close_connection()

    def my_list_converter(self, object):
        if isinstance(object, list):
            return ', '.join(map(str, object))
        return object

    def my_standard_converter(self, object):
        if isinstance(object, datetime):
            return object.strftime(self.format.format_datetime__d_m_Y_H_M_S)
        elif isinstance(object, time):
            return object.strftime(self.format.format_datetime__H_M_S)

    def perfect_eval(self, anonstring):
        try:
            ev = ast.literal_eval(anonstring)
            return ev
        except ValueError:
            corrected = "\'" + anonstring + "\'"
            ev = ast.literal_eval(corrected)
            return ev

    def result_to_json(self, status, message):
        return json.dumps(
            {self.params.status: status, self.params.message: message},
            ensure_ascii=False
        ).encode(self.params.utf8).decode()

    def result_to_extra_json(self, dictionary):
        return json.dumps(
            dictionary,
            ensure_ascii=False
        ).encode(self.params.utf8).decode()

    def cron_description(self, cron_time):
        options = Options()
        options.throw_exception_on_parse_error = True
        options.casing_type = CasingTypeEnum.Sentence
        options.use_24hour_time_format = True
        options.locale_code = 'es_ES'
        descripter = ExpressionDescriptor(cron_time, options)
        return descripter.get_description(DescriptionTypeEnum.FULL)

    def json_cron_description(self, request):
        parameters = help_func.parameters_to_dict(request)
        result = self.cron_description(
            parameters[self.params.cron_time]
        )
        status = self.status_class.status_OK
        return json.dumps(
            {self.params.status: status, self.params.message: result},
            ensure_ascii=False
        ).encode(self.params.utf8).decode()

    def shutdown_cron(self, object_cron):
        # object_cron.scheduler.shutdown()
        list_jobs = [job.id for job in object_cron.scheduler.get_jobs()]
        for job in list_jobs: object_cron.scheduler.remove_job(job)
        return json.dumps(
            {
                self.params.status: self.status_class.status_OK,
                self.params.message: self.message_class.message_cron_stopped
            },
            ensure_ascii=False
        ).encode(self.params.utf8).decode()
