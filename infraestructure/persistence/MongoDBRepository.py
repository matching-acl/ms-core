# -- coding: UTF-8 --
from interface import implements
from core.abstractions.mongo.IMongoRepository import IMongoRepository
from pymongo import MongoClient


class MongoDBRepository(implements(IMongoRepository)):

    def __init__(self):
        self._connection = ""
        self._collection = "local2"
        self._document = "interface4"
        self._client = MongoClient()
        self._db = None
        self.make_connection()

    def run_conciliation(self, collections):

        cursor = self.get_object()
        tweets_collection = list(cursor[:])
        for documents in tweets_collection:
            # print(documents[c[c0]])
            db = self._db[collections[1]["document"]].find({"a": {collections[1]["key"]: "c1"}})

        pass

    def put_object(self):
        pass

    def make_connection(self, collection=""):
        if self._db is None:
            collection = collection if collection != "" else self._collection
            self._db = self._client[collection]
        return self._db

    def get_object(self, document="", filters=""):
        document = document if document != "" else self._document
        filters = filters if filters != "" else {}
        cursor = self._db[document].find(filters)
        return cursor

    def get_all_object(self, document=""):
        document = document if document != "" else self._document
        cursor = self._db[document].find({})
        return cursor

    def list_objects(self):
        pass

    def remove_object(self):
        pass

