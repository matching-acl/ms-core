import json
import traceback

import requests
import inject
from apscheduler.triggers.cron import CronTrigger
from interface import implements

from core.abstractions.postgres.IConfConciliationRepository import IConfConciliationRepository
from core.abstractions.postgres.ICronRepository import ICronRepository
from infraestructure.persistence.CronRepository import CronRepository
from core.configuration.PostgresConnection import PostgresConnection
from infraestructure.resources.message_response import MessageResponse
from infraestructure.resources.model_resource import ModelResource
from infraestructure.resources.params_resource import ParamsResource
from infraestructure.resources.prefix_resource import PrefixResource
from infraestructure.resources.status_response import StatusResponse
from infraestructure.resources.api_rest_urls import ApiRestURLs
from service.postgres import help_func


class ConfConciliationRepository(implements(IConfConciliationRepository)):

    def __init__(self):
        self.__connection = None
        self.__mongodb_connection = None
        self.message_class = MessageResponse()
        self.status_class = StatusResponse()
        self.models = ModelResource()
        self.params = ParamsResource()
        self.api_rest = ApiRestURLs()
        self.prefixs = PrefixResource()

    def make_connection(self):
        self.__connection = PostgresConnection()
        return self.__connection

    def close_connection(self):
        self.__connection.close()

    # def make_mongodb_connection(self):
    #     self.__mongodb_connection = MongoDBConnection()
    #     return self.__mongodb_connection
    #
    # def close_mongodb_connection(self):
    #     self.__mongodb_connection.close_connection()

    def put_object(self, request, i_postgres_repository, object_cron):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.id_name, self.params.object]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                id_name, object = parameters[self.params.id_name], parameters[self.params.object]
                if schema == "" or model == "" or id_name == "" or not object or object == "":
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                object_new = object.copy()
                filters = object.copy()
                if not isinstance(object, dict):
                    object_new = json.loads(object)
                    filters = json.loads(object)
                if model == self.models.model_conf_conciliation:
                    # del filters[self.params.header]
                    name = help_func.convert_valid_names(object_new[self.params.name])
                    unique = {self.params.name: name}
                    if i_postgres_repository.find_object(schema=schema, model=model, filters=unique):
                        result = self.message_class.message_exists_object
                        status = self.status_class.status_error
                        return i_postgres_repository.result_to_json(status, result)
                    code = i_postgres_repository.available_code(object_new[self.params.name], schema, model)
                    object_new[self.params.code] = code
                    object_new[self.params.name] = name
                    json_keys = {self.params.keys : object_new[self.params.keys]}
                    for i in range(len(json_keys[self.params.keys])):
                        for j in range(len(json_keys[self.params.keys][i])):
                            item = {
                                k: (0 if v is None else v) for k, v in json_keys[self.params.keys][i][j].items()
                            }
                            json_keys[self.params.keys][i][j] = item

                    object_new[self.params.keys] = json_keys[self.params.keys]

                    object_new[self.params.keys] = i_postgres_repository.my_list_converter(
                        object_new[self.params.keys]
                    )

                    object_new[self.params.keys] = '[' + help_func.replacer(
                        object_new[self.params.keys], ["'", self.params.true_upper], ['"', self.params.true_lower]
                    ) + ']'
                    object_new[self.params.interfaces] = i_postgres_repository.my_list_converter(
                        object_new[self.params.interfaces]
                    )
                    object_new[self.params.interfaces] = '[' + help_func.replacer(
                        object_new[self.params.interfaces], ["'", self.params.true_upper], ['"', self.params.true_lower]
                    ) + ']'
                    object_new[self.params.cron_time_text] = i_postgres_repository.cron_description(
                        object_new[self.params.cron_time]
                    )
                    cron_time = object_cron.evaluate_cron_expresion(object_new[self.params.cron_time])
                    object_new[self.params.cron_time_out] = object_cron.cron_after_minutes(cron_time, 30)
                    object_new[self.params.cron_time_out_text] = i_postgres_repository.cron_description(
                        object_new[self.params.cron_time_out]
                    )
                    if self.params.out_validated in object_new:
                        if self.params.outgoings_templates in object_new:
                            object_new[self.params.outgoings_templates] = i_postgres_repository.my_list_converter(
                                object_new[self.params.outgoings_templates]
                            )
                            object_new[self.params.outgoings_templates] = '[' + help_func.replacer(
                                object_new[self.params.outgoings_templates], ["'"],['"']
                            ) + ']'
                    conn = self.make_connection()
                    conn.set_cursor_dictionary()
                    cursor = conn.cursor
                    try:
                        query = 'SELECT MAX(' + schema + '.' + model + '.' + id_name + ') FROM ' + schema + '.' + model
                        cursor.execute(query)
                        actual_id = cursor.fetchone()[0]
                        object_new[id_name] = '1'
                        if actual_id:
                            object_new[id_name] = str(actual_id + 1)
                        fields = ', '.join(object_new.keys())
                        values = ', '.join(['%%(%s)s' % x for x in object_new])
                        query = 'INSERT INTO ' + schema + '.' + model + ' (%s) VALUES (%s)' % (fields, values)
                        result = i_postgres_repository.execute(conn, cursor, query, object_new)
                        status = self.status_class.status_OK
                        conn.close()
                        self.close_connection()

                        # INICIO DE EJECUCION DEL JOB

                        job_id = schema + '_' + object_new[self.params.code] + self.prefixs.suffix_conciliation
                        print('JOB ID: ' + job_id)
                        cron_time = object_cron.evaluate_cron_expresion(object_new[self.params.cron_time])
                        print('EJECUCION CONCILIACION: ' + object_new[self.params.cron_time_text])

                        object_cron.scheduler.add_job(
                            lambda: self.process_job_conciliation(
                                parameters, object_new
                            ), CronTrigger.from_crontab(
                                cron_time
                            ), id=job_id
                        )
                        # if len(object_cron.scheduler.get_jobs()) == 1:
                        #     object_cron.scheduler.start()
                        if object_new[self.params.out_validated]:
                            cron_time_out = object_cron.cron_after_minutes(cron_time, 30)
                            print('JOB ID (SALIDA CONCILIACION): ' + 'out_' + job_id)
                            print('EJECUCION SALIDA CONCILIACION: ' + cron_time_out)
                            object_cron.scheduler.add_job(
                                lambda: self.process_job_out_conciliation(
                                    parameters, object_new
                                ), CronTrigger.from_crontab(
                                    cron_time_out
                                ), id='out_' + job_id
                            )

                    except Exception as e:
                        print(e)
                        result = e
                        status = self.status_class.status_error
                        conn.close()
                        self.close_connection()
                else:
                    extra_message = self.message_class.message_extra_schema_model
                    result = self.message_class.message_invalids_params + extra_message
                    status = self.status_class.status_error
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result, status = "ACCESO DENEGADO", "400"
        return i_postgres_repository.result_to_json(status, result)

    def change_object(self, request, i_postgres_repository, object_cron):
        is_editable_active = False
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.id_name, self.params.object_new]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                id_name, object_new = parameters[self.params.id_name], parameters[self.params.object_new]
                if schema == "" or model == "" or id_name == "" or not object_new:
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                object = object_new.copy()
                filters = object_new.copy()
                if not isinstance(object_new, dict):
                    object = json.loads(object_new)
                    filters = json.loads(object_new)
                del filters[self.params.id]
                if model == self.models.model_conf_conciliation:
                    if self.params.name in object:
                        # object[self.params.code] = help_func.coder(object[self.params.name])
                        name = help_func.convert_valid_names(object[self.params.name])
                        unique = {self.params.name: name}
                        if i_postgres_repository.find_object(schema=schema, model=model, filters=unique):
                            value = json.loads(
                                i_postgres_repository.find_object(schema=schema, model=model, filters=unique)
                            )
                            if int(value[id_name]) != int(object_new[self.params.id]):
                                result = self.message_class.message_exists_object
                                status = self.status_class.status_error
                                return i_postgres_repository.result_to_json(status, result)
                        object[self.params.name] = name
                    if self.params.keys in object:
                        json_keys = {self.params.keys: filters[self.params.keys]}
                        for i in range(len(json_keys[self.params.keys])):
                            for j in range(len(json_keys[self.params.keys][i])):
                                item = {
                                    k: (0 if v is None else v) for k, v in json_keys[self.params.keys][i][j].items()
                                }
                                json_keys[self.params.keys][i][j] = item

                        filters[self.params.keys] = json_keys[self.params.keys]
                        keys = str(i_postgres_repository.my_list_converter(filters[self.params.keys]))
                        keys = help_func.replacer(
                            keys, ["'", self.params.true_upper], ['"', self.params.true_lower]
                        )
                        object[self.params.keys] = '[' + keys + ']'
                        del filters[self.params.keys]
                    if self.params.interfaces in object:
                        interfaces = str(i_postgres_repository.my_list_converter(filters[self.params.interfaces]))
                        interfaces = help_func.replacer(
                            interfaces, ["'", self.params.true_upper], ['"', self.params.true_lower]
                        )
                        object[self.params.interfaces] = '[' + interfaces + ']'
                        del filters[self.params.interfaces]
                    if self.params.outgoings_templates in object:
                        outgoings_templates = str(i_postgres_repository.my_list_converter(
                            filters[self.params.outgoings_templates])
                        )
                        outgoings_templates = help_func.replacer(
                            outgoings_templates, ["'"], ['"']
                        )
                        object[self.params.outgoings_templates] = '[' + outgoings_templates + ']'
                        del filters[self.params.outgoings_templates]
                    if self.params.cron_time in object:
                        object[self.params.cron_time_text] = i_postgres_repository.cron_description(
                            object[self.params.cron_time]
                        )
                        cron_time = object_cron.evaluate_cron_expresion(object[self.params.cron_time])
                        object[self.params.cron_time_out] = object_cron.cron_after_minutes(cron_time, 30)
                        object_new[self.params.cron_time_out_text] = i_postgres_repository.cron_description(
                            object[self.params.cron_time_out]
                        )
                    if self.params.active in object:
                        if object[self.params.active]:
                            if not self.validate_activation(parameters, i_postgres_repository):
                                result = self.message_class.message_cannot_activate_extra_model.format(
                                    self.models.model_conf_conciliation.upper(),
                                    self.models.model_interfaces.upper(),
                                    self.models.model_templates.upper()
                                )
                                status = self.status_class.status_error
                                return i_postgres_repository.result_to_json(status, result)
                            is_editable_active = True
                        else:
                            unique = {self.params.id: object[self.params.id]}
                            element = json.loads(
                                i_postgres_repository.find_object(schema=schema, model=model, filters=unique)
                            )
                            job_id = schema + '_' + element[self.params.code] + self.prefixs.suffix_conciliation
                            if object_cron.scheduler.get_job(job_id):
                                object_cron.scheduler.remove_job(job_id)
                            if object_cron.scheduler.get_job('out_' + job_id):
                                object_cron.scheduler.remove_job('out_' + job_id)
                            object[self.params.out_validated] = 0
                    conn = self.make_connection()
                    conn.set_cursor_dictionary()
                    cursor = conn.cursor
                    try:
                        query = ""
                        id = object[self.params.id]
                        for keys, values in zip(object.keys(),
                                                ['%%(%s)s' % i_postgres_repository.my_list_converter(x) for x in object]):
                            query = f"{query}, {keys} = {values}" if query else f"{keys} = {values}"
                        query = 'UPDATE ' + schema + '.' + model + ' SET ' + query + ' WHERE ' + schema + '.' + model + '.' + id_name + ' = (%s)' % (
                            id)
                        result = i_postgres_repository.execute(conn, cursor, query, object)
                        status = self.status_class.status_OK
                        conn.close()
                        self.close_connection()

                        # RESTRUCTURACION DEL JOB

                        unique = {self.params.id: id}
                        element = json.loads(
                            i_postgres_repository.find_object(schema=schema, model=model, filters=unique)
                        )
                        job_id = schema + '_' + element[self.params.code] + self.prefixs.suffix_conciliation
                        cron_time = object_cron.evaluate_cron_expresion(element[self.params.cron_time])
                        element[self.params.schema] = schema

                        # if len(object_cron.scheduler.get_jobs()) >= 1:
                        if (job_id in [i.id for i in object_cron.scheduler.get_jobs()] \
                            and not self.params.active in object) or is_editable_active or \
                            job_id not in [i.id for i in object_cron.scheduler.get_jobs()]:
                            if object_cron.scheduler.get_job(job_id):
                                object_cron.scheduler.remove_job(job_id)
                            print('JOB ID (CONCILIACION EDITADA): ' + job_id)
                            print('EJECUCION CONCILIACION: ' + element[self.params.cron_time_text])
                            object_cron.scheduler.add_job(
                                lambda: self.process_job_conciliation(
                                    parameters, element
                                ), CronTrigger.from_crontab(
                                    cron_time
                                ), id=job_id
                            )
                            # if len(object_cron.scheduler.get_jobs()) == 1:
                            #     object_cron.scheduler.start()
                        if ('out_' + job_id in [i.id for i in object_cron.scheduler.get_jobs()] \
                                and not self.params.active in object) or is_editable_active or \
                                'out_' + job_id not in [i.id for i in object_cron.scheduler.get_jobs()]:
                                if object_cron.scheduler.get_job('out_' + job_id):
                                    object_cron.scheduler.remove_job('out_' + job_id)
                                if element[self.params.out_validated] == 1:
                                    cron_time_out = object_cron.cron_after_minutes(cron_time, 30)
                                    print('JOB ID (SALIDA CONCILIACION EDITADA): ' + 'out_' + job_id)
                                    print('EJECUCION SALIDA CONCILIACION: ' + cron_time_out)
                                    object_cron.scheduler.add_job(
                                        lambda: self.process_job_out_conciliation(
                                            parameters, element
                                        ), CronTrigger.from_crontab(
                                            cron_time_out
                                        ), id='out_' + job_id
                                    )

                    except Exception as e:
                        print(e)
                        result = e
                        status = self.status_class.status_error
                        conn.close()
                        self.close_connection()
                else:
                    extra_message = self.message_class.message_extra_schema_model
                    result = self.message_class.message_invalids_params + extra_message
                    status = self.status_class.status_error
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result, status = "ACCESO DENEGADO", "400"
        return i_postgres_repository.result_to_json(status, result)

    def list_objects(self, request, i_postgres_repository):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                attrs = parameters[self.params.attrs] if self.params.attrs in parameters else None
                orders = parameters[self.params.orders] if self.params.orders in parameters \
                                                        else {self.params.updated : self.params.DESC}
                filters = parameters[self.params.filters] if self.params.filters in parameters else None
                if schema == "" or model == "":
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                query_selectors = self.query_selectors_read(schema)
                query_orders = '' if not orders else ' ORDER BY ' + ', '.join(
                    '{} {}'.format(key, value) for key, value in orders.items())
                query_filters = '' if not filters else ' WHERE ' + ' AND '.join(
                    ['' + key + ' = %s' for key in filters.keys()])
                query_join = self.query_joins_read(schema)
                query = 'SELECT ' + query_selectors + ' FROM ' + schema + '.' + model + query_join + query_filters + query_orders
                conn = self.make_connection()
                conn.set_cursor_dictionary()
                cursor = conn.cursor
                try:
                    filter_values = None if not filters else [value for value in filters.values()]
                    cursor.execute(query, filter_values)
                    results = [i for i in cursor.fetchall()]
                    fields_names = [i[0] for i in cursor.description] if not attrs else attrs
                    data = []
                    for dato in results:
                        value = dict(zip(fields_names, dato))
                        value = {k: ('' if v is None else v) for k, v in value.items()}
                        data.append(value)
                    # total_items = len(data)
                    list_extra_parameters = [self.params.size_page, self.params.number_page]
                    if all(item in parameters_names for item in list_extra_parameters):
                        number_page = parameters[self.params.number_page]
                        size_page = parameters[self.params.size_page]
                        data = data[(number_page * size_page) - size_page: number_page * size_page]
                    data = json.dumps(
                        data,
                        ensure_ascii=False, default=i_postgres_repository.my_standard_converter
                    ).encode(self.params.utf8).decode()
                    conn.close()
                    self.close_connection()
                    data = str(data).replace("\\" + '"', '"')\
                        .replace('"[', '[')\
                        .replace(']"', ']')\
                        .replace('null',"")
                    # response = "{}{}{}{}: {}, {}{}{}: {} {}".format(
                    #     '{', '"', self.params.total_items, '"', str(total_items), '"', self.params.data, '"', data, '}'
                    # )
                    return data
                except Exception as e:
                    print(e)
                    result = e
                    status = self.status_class.status_error
                    conn.close()
                    self.close_connection()
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
    # else:
    #     result, status = 'ACCESO DENEGADO', '400'
        return i_postgres_repository.result_to_json(status, result)

    def get_object(self, request, i_postgres_repository):
        self.message_class = MessageResponse()
        self.status_class = StatusResponse()
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.id_name, self.params.id]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                id_name, id = parameters[self.params.id_name], str(parameters[self.params.id])
                attrs = parameters[self.params.attrs] if self.params.attrs in parameters else None
                if schema == "" or model == "" or id_name == "" or not id:
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                query_selectors = self.query_selectors_read(schema)
                query_join = self.query_joins_read(schema)
                query = 'SELECT ' + query_selectors + ' FROM ' + schema + '.' + model + query_join + ' WHERE ' + schema + '.' + model + '.' + id_name + ' = ' + id
                conn = self.make_connection()
                conn.set_cursor_dictionary()
                cursor = conn.cursor
                try:
                    cursor.execute(query)
                    result = list(cursor.fetchone())
                    fields_names = [i[0] for i in cursor.description] if not attrs else attrs
                    data = []
                    value = dict(zip(fields_names, result))
                    value = {k: ('' if v is None else v) for k, v in value.items()}
                    data.append(value)
                    data = json.dumps(data[0],
                                      ensure_ascii=False,
                                      default=i_postgres_repository.my_standard_converter
                                      ).encode(self.params.utf8).decode()
                    conn.close()
                    self.close_connection()
                    return str(data).replace("\\" + '"', '"').replace('"[', '[').replace(']"', ']').replace('null', '')
                except Exception as e:
                    print(e)
                    result = e
                    status = self.status_class.status_error
                    conn.close()
                    self.close_connection()
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result = self.message_class.message_denied_access
        #     status = self.status_class.status_error
        return i_postgres_repository.result_to_json(status, result)

    def query_selectors_read(self, schema):
        query_selectors = schema + '.conf_conciliation.id as id, '
        query_selectors += schema + '.conf_conciliation.name as name, '
        query_selectors += schema + '.conf_conciliation.code as code, '
        query_selectors += schema + '.conf_conciliation.created as created, '
        query_selectors += schema + '.conf_conciliation.updated as updated, '
        query_selectors += schema + '.conf_conciliation.active as active, '
        query_selectors += schema + '.conf_conciliation.cron_time as cron_time, '
        query_selectors += schema + '.conf_conciliation.cron_time_text as cron_time_text, '
        query_selectors += schema + '.conf_conciliation.cron_time_out as cron_time_out, '
        query_selectors += schema + '.conf_conciliation.cron_time_out_text as cron_time_out_text, '
        query_selectors += schema + '.conf_conciliation.type_conciliation as type_conciliation, '
        query_selectors += schema + '.conf_conciliation.out_validated as out_validated, '
        query_selectors += schema + '.conf_conciliation.interfaces as interfaces, '
        query_selectors += schema + '.conf_conciliation.outgoings_templates as outgoings_templates, '
        query_selectors += schema + '.conf_conciliation.outgoings_id as outgoings_id, '
        query_selectors += schema + '.conf_conciliation.keys as keys, '
        query_selectors += schema + '.conf_conciliation.user_id as user_id, '
        query_selectors += 'private_login.users.first_name as first_name, '
        query_selectors += 'private_login.users.last_name as last_name, '
        query_selectors += 'private_login.users.username as username '
        # query_selectors += schema + '.users.first_name as first_name, '
        # query_selectors += schema + '.users.last_name as last_name, '
        # query_selectors += schema + '.users.username as username '
        return query_selectors

    def query_joins_read(self, schema):
        query_join = ' LEFT JOIN ' + 'private_login.users_relations ON ' + schema + '.conf_conciliation.user_id = private_login.users_relations.id '
        query_join += ' LEFT JOIN ' + 'private_login.users ON private_login.users_relations.id_user = private_login.users.id '
        # query_join = ' LEFT JOIN ' + schema + '.users ON ' + schema + '.conf_conciliation.user_id = ' + schema + '.users.id '
        return query_join

    def validate_activation(self, parameters, i_postgres_repository):
        schema = parameters[self.params.schema]
        cconciliation_id = parameters[self.params.object_new][self.params.id]
        model = self.models.model_conf_conciliation
        unique = {self.params.id: cconciliation_id}
        cconciliation = json.loads(i_postgres_repository.find_object(schema=schema, model=model, filters=unique))
        interfaces_cc = eval(cconciliation[self.params.interfaces])
        m_interfaces = self.models.model_interfaces
        m_templates = self.models.model_templates
        for i in interfaces_cc:
            unique = {self.params.id: i[self.params.interfaceId]}
            interface = json.loads(
                i_postgres_repository.find_object(schema=schema, model=m_interfaces, filters=unique)
            )
            unique = {self.params.id: interface[self.params.template_id]}
            template = json.loads(
                i_postgres_repository.find_object(schema=schema, model=m_templates, filters=unique)
            )
            if not interface[self.params.active] or not template[self.params.active]:
                return False
        return True

    def validate_inactivation(self, parameters, i_postgres_repository):
        # MATAR EL CRON
        pass

    def rollback_error(self):
        conn = self.make_connection()
        conn.set_cursor_dictionary()
        cursor = conn.cursor
        cursor.execute(self.message_class.message_execute_rollback)

    def dict_replace_none_values(self, dict):
        result = {}
        for key, value in dict:
            if value is None:
                value = ''
            result[key] = value
        return result

    def request_for_load_data_job(self, parameters, object):
        return json.dumps({
            self.params.schema: parameters[self.params.schema],
            self.params.code: object[self.params.code],
            self.params.interfaces: object[self.params.interfaces],
            self.params.keys: object[self.params.keys]
        })

    def process_job_conciliation(self, parameters, object):
        # print('HELLO KITTY CONCILIATION...')
        headers = {'Content-type': self.params.content_type, 'Accept': self.params.content_type}
        print(self.request_for_load_data_job(parameters, object))
        try:
            result = requests.post(
                self.api_rest.prod_load_data_host + self.api_rest.route_c_conciliation,
                # self.api_rest.dev_load_data_host + self.api_rest.route_load_data,
                data=self.request_for_load_data_job(parameters, object),
                headers=headers
            )
        except:
            tb = 'ERROR PROCESSING CONCILIATION: ' + traceback.format_exc()
        else:
            tb = 'PROCESSING CONCILIATION: ' + str(result.json())
        finally:
            print(tb)

    def process_job_out_conciliation(self, parameters, object):
        print('Proceso de ejecucion de la salida de la conciliacion...')
            