import inspect
import json
import pysftp
from interface import implements
from core.abstractions.postgres.ISFTPRepository import ISFTPRepository
from infraestructure.resources.message_response import MessageResponse
from infraestructure.resources.model_resource import ModelResource
from infraestructure.resources.params_resource import ParamsResource
from infraestructure.resources.status_response import StatusResponse
from service.postgres import help_func
from ftplib import FTP


class SFTPRepository(implements(ISFTPRepository)):

    def __init__(self):
        self.message_class = MessageResponse()
        self.status_class = StatusResponse()
        self.models = ModelResource()
        self.params = ParamsResource()

    def connection(self, username, password, url, port, e_origin):
        try:
            if e_origin.upper() == 'SFTP':
                cnopts = pysftp.CnOpts(knownhosts='known_hosts')
                cnopts.hostkeys = None
                sftp_connection = SFTPConnection(
                    host=url, username=username, password=password, port=port, cnopts=cnopts
                )
                sftp_connection.timeout = 10
                with  sftp_connection as sftp:
                    return len(sftp.listdir()) > 0
            elif e_origin.upper() == 'FTP':
                ftp = FTP()
                ftp.login(user=username, passwd=password)
                return ftp.connect(host=url, port=port)
        except Exception as exception:
            print(str(exception))
            return False

    def is_connected(self, username, password, url, port, e_origin):
        return self.connection(username, password, url, port, e_origin)

    def sftp_test_connection(self, request, i_postgres_repository):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        function_parameters = list(inspect.getfullargspec(self.is_connected).args)
        check = all(item in function_parameters for item in parameters_names)
        list_valid_names = [
            self.params.username, self.params.password, self.params.url, self.params.port, self.params.e_origin
        ]
        if check is True:
            try:
                if help_func.format_valid_params_values(parameters, function_parameters, list_valid_names):
                    response = True if self.is_connected(**parameters) else False
                    result = json.dumps(response)
                    print("SFTP CONNECTION RESULT: " + result)
                    status = self.status_class.status_OK
                else:
                    extra_message = self.message_class.message_extra_sftp
                    result = self.message_class.message_invalids_params + extra_message
                    status = self.status_class.status_error
            except Exception as e:
                result = self.message_class.message_invalids_params
                status = self.status_class.status_error
                return i_postgres_repository.result_to_json(status, result)
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # return 'ACCESO DENEGADO'
        return i_postgres_repository.result_to_json(status, result)


class SFTPConnection(pysftp.Connection):

    def __init__(self, *args, **kwargs):
        self._sftp_live = False
        self._transport = None
        super().__init__(*args, **kwargs)
