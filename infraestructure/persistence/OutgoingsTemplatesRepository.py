import json
from interface import implements
from psycopg2._psycopg import IntegrityError, InterfaceError

from core.abstractions.postgres.IOutgoingsTemplatesRepository import IOutgoingsTemplatesRepository
from core.configuration.PostgresConnection import PostgresConnection
from infraestructure.resources.message_response import MessageResponse
from infraestructure.resources.model_resource import ModelResource
from infraestructure.resources.params_resource import ParamsResource
from infraestructure.resources.status_response import StatusResponse
from service.postgres import help_func


class OutgoingsTemplatesRepository(implements(IOutgoingsTemplatesRepository)):

    def __init__(self):
        self.__connection = None
        self.message_class = MessageResponse()
        self.status_class = StatusResponse()
        self.models = ModelResource()
        self.params = ParamsResource()

    def make_connection(self):
        self.__connection = PostgresConnection()
        return self.__connection

    def close_connection(self):
        self.__connection.close()

    def put_object(self, request, i_postgres_repository):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.object]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                object = parameters[self.params.object]
                if schema == "" or model == "" or not object or object == "":
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                object_new = object.copy()
                filters = object.copy()
                if not isinstance(object, dict):
                    object_new = json.loads(object)
                    filters = json.loads(object)
                if model == self.models.model_outgoings_templates:
                    # VALIDANDO QUE EL NOMBRE NO SE REPITA
                    del filters[self.params.header]
                    name = help_func.convert_valid_names(object_new[self.params.name])
                    unique = {self.params.name: object_new[self.params.name]}
                    if i_postgres_repository.find_object(schema=schema, model=model, filters=unique):
                        result = self.message_class.message_exists_object
                        status = self.status_class.status_error
                        return i_postgres_repository.result_to_json(status, result)
                    # VALIDANDO CODIGO DISPONIBLE PARA OUTGOINGS_TEMPLATES
                    code = 'out_' + i_postgres_repository.available_code(object_new[self.params.name], schema, model)
                    object_new[self.params.code] = code
                    object_new[self.params.name] = name
                    # EFECTUANDO INSERCION
                    conn = self.make_connection()
                    conn.set_cursor_dictionary()
                    cursor = conn.cursor
                    try:
                        query = 'SELECT MAX(' + schema + '.' + model + '.' + self.params.id + ') FROM ' + schema + '.' + model
                        cursor.execute(query)
                        actual_id = cursor.fetchone()[0]
                        object_new[self.params.id] = '1'
                        if actual_id:
                            object_new[self.params.id] = str(actual_id + 1)
                        json_headers = {self.params.header : object_new[self.params.header]}
                        for i in range(len(json_headers[self.params.header])):
                            json_headers[self.params.header][i][self.params.code] = help_func.coder_templates(
                                json_headers[self.params.header][i][self.params.name]
                            )
                        object_new[self.params.header] = json_headers[self.params.header]
                        object_new[self.params.header] = i_postgres_repository.my_list_converter(
                            object_new[self.params.header]
                        )
                        object_new[self.params.header] = '[' + help_func.replacer(
                            object_new[self.params.header], ["'", self.params.true_upper],['"', self.params.true_lower]
                        ) + ']'
                        fields = ', '.join(object_new.keys())
                        values = ', '.join(['%%(%s)s' % x for x in object_new])
                        query = 'INSERT INTO ' + schema + '.' + model + ' (%s) VALUES (%s)' % (fields, values)
                        result = i_postgres_repository.execute(conn, cursor, query, object_new)
                        if isinstance(result, IntegrityError):
                            result = self.message_class.message_exists_object
                            status = self.status_class.status_error
                        else:
                            status = self.status_class.status_OK
                        conn.close()
                        self.close_connection()
                    except Exception as e:
                        print(e)
                        result = e
                        status = self.status_class.status_error
                        conn.close()
                        self.close_connection()
                else:
                    extra_message = self.message_class.message_extra_schema_model
                    result = self.message_class.message_invalids_params + extra_message
                    status = self.status_class.status_error
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result, status = "ACCESO DENEGADO", "400"
        return i_postgres_repository.result_to_json(status, result)

    def change_object(self, request, i_postgres_repository):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.object_new]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                object_new = parameters[self.params.object_new]
                if schema == "" or model == "" or not object_new:
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                object = object_new.copy()
                filters = object_new.copy()
                if not isinstance(object_new, dict):
                    object = json.loads(object_new)
                    filters = json.loads(object_new)
                del filters[self.params.id]
                if model == self.models.model_outgoings_templates:
                    if self.params.header in object:
                        json_headers = {self.params.header: filters[self.params.header]}
                        for i in range(len(json_headers[self.params.header])):
                            json_headers[self.params.header][i][self.params.code] = help_func.coder_templates(
                                json_headers[self.params.header][i][self.params.name]
                            )
                        filters[self.params.header] = json_headers[self.params.header]
                        header = str(i_postgres_repository.my_list_converter(filters[self.params.header]))
                        header = help_func.replacer(
                            header, ["'", self.params.true_upper], ['"', self.params.true_lower]
                        )
                        object[self.params.header] = '[' + header + ']'
                        del filters[self.params.header]
                    if self.params.name in object:
                        # object[self.params.code] = help_func.coder(object[self.params.name])
                        name = help_func.convert_valid_names(object[self.params.name])
                        unique = {self.params.name: name}
                        if i_postgres_repository.find_object(schema=schema, model=model, filters=unique):
                            value = json.loads(
                                i_postgres_repository.find_object(schema=schema, model=model, filters=unique)
                            )
                            if int(value[self.params.id]) != int(object_new[self.params.id]):
                                result = self.message_class.message_exists_object
                                status = self.status_class.status_error
                                return i_postgres_repository.result_to_json(status, result)
                        object[self.params.name] = name
                    conn = self.make_connection()
                    conn.set_cursor_dictionary()
                    cursor = conn.cursor
                    try:
                        query = ""
                        id = object[self.params.id]
                        for keys, values in zip(object.keys(),
                                                ['%%(%s)s' % i_postgres_repository.my_list_converter(x) for x in object]):
                            query = f"{query}, {keys} = {values}" if query else f"{keys} = {values}"
                        query = 'UPDATE ' + schema + '.' + model + ' SET ' + query + ' WHERE ' + schema + '.' + model + '.' + self.params.id + ' = (%s)' % (
                            id)
                        result = i_postgres_repository.execute(conn, cursor, query, object)
                        if isinstance(result, IntegrityError):
                            result = self.message_class.message_exists_object
                            status = self.status_class.status_error
                        else:
                            status = self.status_class.status_OK
                        conn.close()
                        self.close_connection()
                    except Exception as e:
                        print(e)
                        result = e
                        status = self.status_class.status_error
                        conn.close()
                        self.close_connection()
                else:
                    extra_message = self.message_class.message_extra_schema_model
                    result = self.message_class.message_invalids_params + extra_message
                    status = self.status_class.status_error
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result, status = "ACCESO DENEGADO", "400"
        return i_postgres_repository.result_to_json(status, result)

    def list_objects(self, request, i_postgres_repository):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                attrs = parameters[self.params.attrs] if self.params.attrs in parameters else None
                orders = parameters[self.params.orders] if self.params.orders in parameters \
                                                        else {self.params.updated : self.params.DESC}
                filters = parameters[self.params.filters] if self.params.filters in parameters else None
                if filters is not None:
                    if self.params.active in filters:
                        filters[schema + '.' + self.models.model_outgoings_templates + '.' + self.params.active] = \
                            filters[self.params.active]
                        del filters[self.params.active]
                if schema == "" or model == "":
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                if self.params.filters in parameters:
                    if self.params.interface_id in filters.keys():
                        filters[schema + '.' + self.models.model_interfaces + '.' + self.params.id] = filters[self.params.interface_id]
                        del filters[self.params.interface_id]
                query_selectors = self.query_selectors_read(schema)
                query_orders = '' if not orders else ' ORDER BY ' + ', '.join(
                    '{} {}'.format(key, value) for key, value in orders.items())
                query_filters = '' if not filters else ' WHERE ' + ' AND '.join(
                    ['' + key + ' = %s' for key in filters.keys()])
                query_join = self.query_joins_read(schema)
                query = 'SELECT ' + query_selectors + ' FROM ' + schema + '.' + model + query_join + query_filters + query_orders
                conn = self.make_connection()
                conn.set_cursor_dictionary()
                cursor = conn.cursor
                try:
                    filter_values = None if not filters else [value for value in filters.values()]
                    cursor.execute(query, filter_values)
                    results = [i for i in cursor.fetchall()]
                    fields_names = [i[0] for i in cursor.description] if not attrs else attrs
                    data = []
                    for dato in results:
                        value = dict(zip(fields_names, dato))
                        value = {k: ('' if v is None else v) for k, v in value.items()}
                        data.append(value)
                    # total_items = len(data)
                    list_extra_parameters = [self.params.size_page, self.params.number_page]
                    if all(item in parameters_names for item in list_extra_parameters):
                        number_page = parameters[self.params.number_page]
                        size_page = parameters[self.params.size_page]
                        data = data[(number_page * size_page) - size_page: number_page * size_page]
                    data = json.dumps(
                        data,
                        ensure_ascii=False, default=i_postgres_repository.my_standard_converter
                    ).encode(self.params.utf8).decode()
                    if conn.connection.closed == 0:
                        conn.close()
                        self.close_connection()
                    data = str(data).replace("\\" + '"', '"') \
                        .replace('"[', '[').replace(']"', ']').replace('null','')
                    # response = "{}{}{}{}: {}, {}{}{}: {} {}".format(
                    #     '{', '"', self.params.total_items, '"', str(total_items), '"', self.params.data, '"', data, '}'
                    # )
                    return data
                # except InterfaceError as exc:
                #     print(exc)
                #     conn = self.make_connection()
                #     conn.set_cursor_dictionary()
                #     cursor = conn.cursor
                #     raise
                except Exception as e:
                    print(e)
                    result = e
                    status = self.status_class.status_error
                    if conn.connection.closed == 0:
                        conn.close()
                        self.close_connection()
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
    # else:
    #     result, status = 'ACCESO DENEGADO', '400'
        return i_postgres_repository.result_to_json(status, result)

    def get_object(self, request, i_postgres_repository):
        parameters = help_func.parameters_to_dict(request)
        # token = request.headers['authorization'].split()[1]
        # if token == 'md53b2b0eef781e8a6aaf7cf0565b780845':
        parameters_names = list(parameters.keys())
        list_valid_names = [self.params.schema, self.params.model, self.params.id]
        check = all(item in parameters_names for item in list_valid_names)
        if check is True:
            if help_func.format_valid_values(parameters):
                schema, model = parameters[self.params.schema], parameters[self.params.model]
                id = str(parameters[self.params.id])
                attrs = parameters[self.params.attrs] if self.params.attrs in parameters else None
                if schema == "" or model == "" or not id:
                    result = self.message_class.message_empty_params
                    status = self.status_class.status_error
                    return i_postgres_repository.result_to_json(status, result)
                query_selectors = self.query_selectors_read(schema)
                query_join = self.query_joins_read(schema)
                query = 'SELECT ' + query_selectors + ' FROM ' + schema + '.' + model + query_join + ' WHERE ' + schema + '.' + model + '.' + self.params.id + ' = ' + id
                conn = self.make_connection()
                conn.set_cursor_dictionary()
                cursor = conn.cursor
                try:
                    cursor.execute(query)
                    result = list(cursor.fetchone())
                    fields_names = [i[0] for i in cursor.description] if not attrs else attrs
                    data = []
                    value = dict(zip(fields_names, result))
                    value = {k: ('' if v is None else v) for k, v in value.items()}
                    data.append(value)
                    data = json.dumps(data[0],
                                      ensure_ascii=False,
                                      default=i_postgres_repository.my_standard_converter
                                      ).encode(self.params.utf8).decode()
                    conn.close()
                    self.close_connection()
                    return str(data).replace("\\" + '"', '"').replace('"[', '[').replace(']"', ']').replace('null', '')
                except Exception as e:
                    print(e)
                    result = e
                    status = self.status_class.status_error
                    conn.close()
                    self.close_connection()
            else:
                extra_message = self.message_class.message_extra_schema_model_idname_object
                result = self.message_class.message_invalids_params + extra_message
                status = self.status_class.status_error
        else:
            result = self.message_class.message_invalids_params
            status = self.status_class.status_error
        # else:
        #     result = self.message_class.message_denied_access
        #     status = self.status_class.status_error
        return i_postgres_repository.result_to_json(status, result)

    def query_selectors_read(self, schema):
        query_selectors = schema + '.outgoings_templates.id as id, '
        query_selectors += schema + '.outgoings_templates.name as name, '
        query_selectors += schema + '.outgoings_templates.code as code, '
        query_selectors += schema + '.outgoings_templates.template_id as tc_template_id, '
        query_selectors += schema + '.outgoings_templates.created as created, '
        query_selectors += schema + '.outgoings_templates.updated as updated, '
        query_selectors += schema + '.outgoings_templates.active as active, '
        query_selectors += schema + '.outgoings_templates.header as header, '
        query_selectors += schema + '.outgoings_templates.user_id as user_id, '
        query_selectors += schema + '.templates.name as template_name, '
        query_selectors += schema + '.templates.code as template_code, '
        query_selectors += schema + '.templates.e_state as template_state, '
        query_selectors += schema + '.interfaces.id as interface_id, '
        query_selectors += 'private_login.users.first_name as first_name, '
        query_selectors += 'private_login.users.last_name as last_name, '
        query_selectors += 'private_login.users.username as username '
        return query_selectors

    def query_joins_read(self, schema):
        query_join = ' LEFT JOIN ' + schema + '.templates ON ' + schema + '.outgoings_templates.template_id = ' + schema + '.templates.id '
        query_join += ' LEFT JOIN ' + schema + '.interfaces ON ' + schema + '.templates.id = ' + schema + '.interfaces.template_id '
        query_join += ' LEFT JOIN ' + 'private_login.users_relations ON ' + schema + '.templates.user_id = private_login.users_relations.id '
        query_join += ' LEFT JOIN ' + 'private_login.users ON private_login.users_relations.id_user = private_login.users.id '
        # query_join += ' LEFT JOIN ' + schema + '.users ON ' + schema + '.templates.user_id = ' + schema + '.users.id '
        return query_join

    def validate_inactivation(self, parameters, i_postgres_repository):
        schema = parameters[self.params.schema]
        template_id = parameters[self.params.object_new][self.params.id]
        model = self.models.model_interfaces
        unique = {self.params.active: True, self.params.template_id: template_id}
        interface = i_postgres_repository.find_object(schema=schema, model=model, filters=unique)
        return interface

    def validate_activation(self, parameters, i_postgres_repository):
        schema = parameters[self.params.schema]
        template_id = parameters[self.params.object_new][self.params.id]
        model = self.models.model_interfaces
        unique = {self.params.active: False, self.params.template_id: template_id}
        interface = i_postgres_repository.find_object(schema=schema, model=model, filters=unique)
        return interface

    def rollback_error(self):
        conn = self.make_connection()
        conn.set_cursor_dictionary()
        cursor = conn.cursor
        cursor.execute(self.message_class.message_execute_rollback)
        self.close_connection()

# postgres_repo = PostgresRepository()
# attrs = {'code'}
# result = json.loads(postgres_repo.find_all(schema='acl_koncilia', model='templates', attrs=attrs))
# result = list(set([x['code'] for x in result]))
# print(result)