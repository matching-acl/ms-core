import os

class ApiRestURLs:
    def __init__(self):

        # self.prod_host = 'http://34.67.4.220:5000'
        # self.dev_host = 'http://127.0.0.1:5000'
        # self.prod_files_host = 'http://34.122.57.142:8080'
        # self.prod_load_data_host = 'http://34.67.160.44:9090'
        # self.dev_load_data_host = 'http://127.0.0.1:9090'

        self.prod_files_host = os.environ['FMANAGEMENT_API_URL']
        self.prod_load_data_host = os.environ['GO_API_URL']

        self.route_read = 'read'
        self.route_create = 'create'
        self.route_update = 'update'
        self.route_get = 'get'
        self.route_dir_create = 'file/copy'
        self.route_load_data = 'load_data'
        self.route_c_conciliation = 'c_conciliation'