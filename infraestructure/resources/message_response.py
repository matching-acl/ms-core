
class MessageResponse:

    def __init__(self):
        self.message_invalids_params = 'PARÁMETROS NO VÁLIDOS'
        self.message_empty_params = 'PARÁMETROS VACÍOS'
        self.message_exists_table = 'TABLA(S) EXISTENTE(S)'
        self.message_error_create_schema = 'ERROR AL CREAR EL SCHEMA'
        self.message_no_lines_to_test = 'ARCHIVO SIN EXISTENCIA DE LINEAS A PROBAR'
        self.message_no_file_test = 'SIN ARCHIVO DE PRUEBA'
        self.message_denied_access = 'ACCESO DENEGADO'
        self.message_invalids_params_values = 'PARÁMETROS Y/O VALORES NO VÁLIDOS'
        self.message_exists_object = 'OBJECT EXISTS'
        self.message_empty_headers = 'HEADERS VACÍOS'
        self.message_execute_rollback = 'ROLLBACK'
        self.message_extra_schema_model = ' (Introduzca valores correctos: schema, model)'
        self.message_extra_schema_model_idname_object = ' (Introduzca: schema, model, id_name, object)'
        self.message_extra_sftp = ' (Introduzca: username, password, url, port)'
        self.message_extra_model = ' (Introduzca valores correctos: model)'

        self.message_cannot_desactivate_model = 'IMPOSIBLE DESACTIVAR {}, {} ASOCIADO(A) ESTA ACTIVO(A)'
        self.message_cannot_desactivate_model_in_use = 'IMPOSIBLE DESACTIVAR {}, ' \
                                                       '{} ASOCIADO(A) ESTA USANDO EL MODELO {}'
        self.message_cannot_activate_model = 'IMPOSIBLE ACTIVAR {}, {} ASOCIADO(A) ESTA DESACTIVADO'
        self.message_cannot_activate_extra_model = 'IMPOSIBLE ACTIVAR {}, {} ASOCIADO(A) Y/O {} ESTA DESACTIVADO'
        self.message_cron_stopped = 'CRON DETENIDO SATISFACTORIAMENTE'