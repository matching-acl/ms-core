
class ModelResource:
    def __init__(self):
        self.model_templates = 'templates'
        self.model_interfaces = 'interfaces'
        self.model_conf_conciliation = 'conf_conciliation'
        self.model_outgoings = 'outgoings'
        self.model_outgoings_templates = 'outgoings_templates'