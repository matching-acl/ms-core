
class ExcelResource:

    def __init__(self):
        self.CAMPO = 'CAMPO'
        self.LONGITUD = 'LONGITUD'

        self.excel_response_mimetype = 'application/ms-excel'
        self.excel_response_headers_content = 'Content-Disposition'
        self.excel_response_headers_type = 'attachment;'
        self.excel_file_name = 'filename='