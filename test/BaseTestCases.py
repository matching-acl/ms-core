# -- coding: UTF-8 --

import inject
import unittest
import os


# Use for https://docs.aws.amazon.com/es_es/amazondynamodb/latest/developerguide/DynamoDBLocal.html
from core.abstractions.mongo.IMongoRepository import IMongoRepository
from infraestructure.persistence.MongoDBRepository import MongoDBRepository


class BaseTestCases(unittest.TestCase):

    def setUp(self):
        # self.set_test_stage()
        self.inject = inject.clear_and_configure(lambda binder: binder.bind(IMongoRepository, MongoDBRepository()))

    def tearDown(self):
        inject.clear()

    def test_Common(self):
        print('Calling BaseTestCases:test_Common')
        value = True
        self.assertEquals(value, True)

    # def set_test_stage(self):
    #     os.environ['FC_CLEAN_ARCHITECTURE_DEMO_ENTITY_TABLE'] = 'fc-cleanarchitecture-python-demo-entity-test'
    #     os.environ["AWS_REGION"] = "us-west-2"
    #     os.environ["AWS_DEFAULT_REGION"] = "us-west-2"
    #     os.environ['AWS_ACCESS_KEY_ID'] = 'AKIAR3GSJLXM4R2HNEFE'
    #     os.environ['AWS_SECRET_ACCESS_KEY'] = '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'
