# -- coding: UTF-8 --

import unittest

from test.BaseTestCases import BaseTestCases
from core.abstractions.mongo.IMongoRepository import IMongoRepository
from core.controllers.MongoController import MongoController


class MongoControllerTest(BaseTestCases):

    # def test_get_object(self):
    #     try:
    #         i_mongo_repository = self.inject.get_instance(IMongoRepository)
    #         mongo_controller = MongoController(i_mongo_repository)
    #         process = mongo_controller.get_object()
    #         self.assertIsNotNone(process, "test_ProcessRecord")
    #         print('test_get_object-> finish')
    #     except Exception as ex:
    #         self.assertRaises(self, ex)

    def test_run_conciliation(self):
        try:
            payload = {
                "conciliation": [
                    {
                        "collection": "local2",
                        "document": "interface4",
                        "key": "c1"
                    },
                    {
                        "collection": "local2",
                        "document": "interface3",
                        "key": "c1"
                    },

                ]
            }
            i_mongo_repository = self.inject.get_instance(IMongoRepository)
            mongo_controller = MongoController(i_mongo_repository)
            process = mongo_controller.run_conciliation(payload)
            self.assertEqual(process, True, "test_ProcessRecord")
            print('test_run_conciliation-> finish')
        except Exception as ex:
            self.assertRaises(self, ex)


if __name__ == '__main__':
    unittest.main()


