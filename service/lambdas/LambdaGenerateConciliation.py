# -- coding: UTF-8 --
from core.abstractions.mongo import IMongoRepository
from core.controllers.MongoController import MongoController
from service.lambdas import LambdaBase

inject = LambdaBase.resolve_dependency_injection()
i_mongo_repository = inject.get_instance(IMongoRepository)


def lambda_generate_conciliation(event, context):

    try:
        print("Init execute handler LambdaGenerateConciliation")
        mongo_controller = MongoController(i_mongo_repository)

        # response = demo_controller.ProcessApiGatewayEvent(payload)
        print("End execute handler LambdaGenerateConciliation")
        # return response
    except Exception as ex:
        error_message = 'Error in LambdaGenerateConciliation. {}'.format(str(ex))
        print(error_message)
        raise

