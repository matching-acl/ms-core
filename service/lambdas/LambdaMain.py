import inject
from flask import Flask
from flask import request
from flask_cors import CORS
from flask_restplus import Api, Resource
from core.abstractions.postgres.IPostgresRepository import IPostgresRepository
from core.abstractions.postgres.ICronRepository import ICronRepository
from infraestructure.persistence.CronRepository import CronRepository
from core.controllers.PostgresController import PostgresController
from core.controllers.SFTPController import SFTPController
from service.flask_restplus_doc import models
from service.lambdas.LambdaBaseAPI import LambdaBaseAPI
from service.postgres import help_func

injection = LambdaBaseAPI.resolve_dependency_injection()
i_postgres_repository = injection.get_instance(IPostgresRepository)
injection_cron = inject.clear_and_configure(
    lambda binder: binder.bind(ICronRepository, CronRepository())
)
object_cron = injection_cron.get_instance(ICronRepository)
object_cron.scheduler.start()
postgres_controller = PostgresController(i_postgres_repository, object_cron)
sftp_controller = SFTPController(i_postgres_repository)

app = Flask(__name__)
cors = CORS(app)
app.config['SECRET_KEY'] = models.secret
api = Api(app, title='ACL Concilia Python APIs', description='APIs definidas...')

# TABLAS (DECLARACION)

ns_templates = api.namespace('templates', description='OPERACIONES CON TEMPLATES')
template = api.model('templates', models.template)
value_template_list = models.value_template_list
# template_list = api.model('templates_list', value_template_list)
value_template_create = models.value_template_create
value_template_update = models.value_template_update

ns_interfaces = api.namespace('interfaces', description='OPERACIONES CON INTERFACES')
interface = api.model('interfaces', models.template)
value_interface_list = models.value_interface_list
# template_list = api.model('templates_list', value_template_list)
value_interface_create = models.value_interface_create
value_interface_update = models.value_interface_update

# ENUMERADORES (DECLARACION)

ns_tc_origin = api.namespace('tc_origin', description='OPERACIONES CON TC ORIGIN (ENUMERADOR)')
value_tc_origin_list = models.value_tc_origin_list
ns_tc_template = api.namespace('tc_template', description='OPERACIONES CON TC TEMPLATES (TABLA)')
value_tc_template_list = models.value_tc_template_list
ns_tc_entity_type = api.namespace('tc_entity_type', description='OPERACIONES CON TC ENTITY TYPES (ENUMERADOR)')
value_tc_entity_type_list = models.value_tc_entity_type_list
ns_tc_file_format = api.namespace('tc_file_format', description='OPERACIONES CON TC FILE FORMAT (ENUMERADOR)')
value_tc_file_format_list = models.value_tc_file_format_list
ns_tc_separator = api.namespace('tc_separator', description='OPERACIONES CON TC SEPARATOR (ENUMERADOR)')
value_tc_separator_list = models.value_tc_separator_list
ns_tc_state = api.namespace('tc_state', description='OPERACIONES CON TC STATE (ENUMERADOR)')
value_tc_state_list = models.value_tc_state_list

# TABLAS (API DOC)

@ns_templates.route('/read')
class TemplatesList(Resource):
    @ns_templates.doc('list_templates')
    @ns_templates.param(help_func.replacer(str(value_template_list), ["'"], ['"']))
    def get(self):
        models.function_process_request(request, value_template_list, app.config['SECRET_KEY'])
        return postgres_controller.list_objects(request)

@ns_templates.route('/create')
class TemplatesCreate(Resource):
    @ns_templates.doc('create_template')
    @ns_templates.param(help_func.replacer(str(value_template_create), ["'"], ['"']), description='VALOR EJEMPLO')
    def post(self):
        models.function_process_request(request, value_template_create, app.config['SECRET_KEY'])
        return postgres_controller.put_object(request)

@ns_templates.route('/update')
class TemplatesUpdate(Resource):
    @ns_templates.doc('update_template')
    @ns_templates.param(help_func.replacer(str(value_template_update), ["'"], ['"']), description='VALOR EJEMPLO')
    def post(self):
        models.function_process_request(request, value_template_update, app.config['SECRET_KEY'])
        return postgres_controller.change_object(request)
    
@ns_interfaces.route('/read')
class InterfacesList(Resource):
    @ns_interfaces.doc('list_interfaces')
    @ns_templates.param(help_func.replacer(str(value_interface_list), ["'"], ['"']))
    def get(self):
        models.function_process_request(request, value_interface_list, app.config['SECRET_KEY'])
        return postgres_controller.list_objects(request)

@ns_interfaces.route('/create')
class InterfacesCreate(Resource):
    @ns_interfaces.doc('create_interface')
    @ns_templates.param(help_func.replacer(str(value_interface_create), ["'"], ['"']), description='VALOR EJEMPLO')
    def post(self):
        models.function_process_request(request, value_interface_create, app.config['SECRET_KEY'])
        return postgres_controller.put_object(request)

@ns_interfaces.route('/update')
class InterfacesUpdate(Resource):
    @ns_interfaces.doc('update_interface')
    @ns_templates.param(help_func.replacer(str(value_interface_update), ["'"], ['"']), description='VALOR EJEMPLO')
    def post(self):
        models.function_process_request(request, value_interface_update, app.config['SECRET_KEY'])
        return postgres_controller.change_object(request)

@ns_tc_template.route('/read')
class TcTemplatesList(Resource):
    @ns_tc_template.doc('list_tc_templates')
    @ns_templates.param(help_func.replacer(str(value_tc_template_list), ["'"], ['"']))
    def get(self):
        models.function_process_request(request, value_tc_template_list, app.config['SECRET_KEY'])
        return postgres_controller.list_objects(request)

# ENUMERADORES (API DOC)

@ns_tc_entity_type.route('/read')
class TcEntityTypeList(Resource):
    @ns_tc_entity_type.doc('list_tc_entity_type')
    @ns_templates.param(help_func.replacer(str(value_tc_entity_type_list), ["'"], ['"']))
    def get(self):
        models.function_process_request(request, value_tc_entity_type_list, app.config['SECRET_KEY'])
        return postgres_controller.list_objects(request)

@ns_tc_origin.route('/read')
class TcOriginList(Resource):
    @ns_tc_origin.doc('list_tc_origin')
    @ns_templates.param(help_func.replacer(str(value_tc_origin_list), ["'"], ['"']))
    def get(self):
        models.function_process_request(request, value_tc_origin_list, app.config['SECRET_KEY'])
        return postgres_controller.list_objects(request)

@ns_tc_state.route('/read')
class TcStateList(Resource):
    @ns_tc_state.doc('list_tc_state')
    @ns_templates.param(help_func.replacer(str(value_tc_state_list), ["'"], ['"']))
    def get(self):
        models.function_process_request(request, value_tc_state_list, app.config['SECRET_KEY'])
        return postgres_controller.list_objects(request)

@ns_tc_file_format.route('/read')
class TcFileFormatList(Resource):
    @ns_tc_file_format.doc('list_tc_file_format')
    @ns_templates.param(help_func.replacer(str(value_tc_file_format_list), ["'"], ['"']))
    def get(self):
        models.function_process_request(request, value_tc_file_format_list, app.config['SECRET_KEY'])
        return postgres_controller.list_objects(request)

@ns_tc_separator.route('/read')
class TcSeparatorList(Resource):
    @ns_tc_separator.doc('list_tc_separator')
    @ns_templates.param(help_func.replacer(str(value_tc_separator_list), ["'"], ['"']))
    def get(self):
        models.function_process_request(request, value_tc_separator_list, app.config['SECRET_KEY'])
        return postgres_controller.list_objects(request)

# RUTAS

@app.route("/read", methods=['POST'])
def list_objects():
    return postgres_controller.list_objects(request)

@app.route("/get", methods=['POST'])
def get_object():
    return postgres_controller.get_object(request)

@app.route("/create", methods=['POST'])
def put_object():
    return postgres_controller.put_object(request)

@app.route("/update", methods=['POST'])
def change_object():
    return postgres_controller.change_object(request)

@app.route("/total_items", methods=['POST'])
def total_items():
    return postgres_controller.total_items(request)

@app.route("/sftp_test_connection", methods=['POST'])
def sftp_test_connection():
    return sftp_controller.sftp_test_connection(request)

@app.route("/execute_cron", methods=['POST'])
def execute_cron():
    return postgres_controller.execute_cron(request)

@app.route("/cron_time_text", methods=['POST'])
def cron_time_text():
    return postgres_controller.cron_time_text(request)

@app.route("/shutdown_cron", methods=['POST'])
def shutdown_cron():
    return postgres_controller.shutdown_cron()

@app.route("/download_empty_template_excel", methods=['GET'])
def download_empty_template_excel():
    return postgres_controller.download_empty_template_excel()

@app.route("/template_to_excel", methods=['POST'])
def template_to_excel():
    return postgres_controller.template_to_excel(request)

@app.route("/upload_template_excel", methods=['POST'])
def upload_template_excel():
    return postgres_controller.upload_template_excel(request)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
