# -- coding: UTF-8 --
import inject

from core.abstractions.mongo import IMongoRepository
from infraestructure.persistence.MongoDBRepository import MongoDBRepository


class LambdaBase(object):
    def __init__(self):
        self.inject = self.resolve_dependency_injection()

    @staticmethod
    def resolve_dependency_injection():
        try:
            return inject.clear_and_configure(lambda binder: binder.bind(IMongoRepository, MongoDBRepository()))

        except Exception as ex:
            error_message = 'Error in resolve_dependency_injection. {}'.format(str(ex))
            print(error_message)
            raise
