import inject

from core.abstractions.postgres.IPostgresRepository import IPostgresRepository
from infraestructure.persistence.PostgresRepository import PostgresRepository


class LambdaBaseAPI(object):
    def __init__(self):
        self.inject = self.resolve_dependency_injection()

    @staticmethod
    def resolve_dependency_injection():
        try:
            return inject.clear_and_configure(lambda binder: binder.bind(IPostgresRepository, PostgresRepository()))

        except Exception as ex:
            error_message = 'Error in resolve_dependency_injection. {}'.format(str(ex))
            print(error_message)
            raise
