from flask_restplus import fields

from service.postgres import help_func

# TEMPLATES

template = {
    'id': fields.Integer(readOnly=True, description='ID único del objeto'),
    'name': fields.String(required=True, description='NOMBRE único del objeto'),
    'tc_template_id': fields.String(required=True, description='ID referencia al objeto/tabla tc_templates'),
    'separator': fields.String(description='SEPARADOR definido'),
    'separator_line': fields.String(description='SEPARADOR entre liíneas definido'),
    'created': fields.DateTime(description='FECHA HORA DE CREADO (VALOR POR DEFECTO EL INSTANTE)'),
    'updated': fields.DateTime(description='FECHA HORA DE ACTUALIZADO (VALOR POR DEFECTO EL INSTANTE)'),
    'active': fields.Boolean(description='VERDADERO O FALSE, si está activo o no'),
    'e_state': fields.DateTime(description='ENUMERADOR ESTADO, referencia al enum tc_state'),
    'header': fields.String(description='LISTADO (en forma de texto) de headers que definen el procedimiento del template')
}

template_list = {
    "is_enum": fields.String(required=True, description='ENUMERADOR ESTADO, toma valor 0 ó 1'),
    "model": fields.String(required=True, description='NOMBRE DEL MODELO, debe ser "templates"'),
    "schema": fields.String(required=True, description='NOMBRE DEL SCHEMA DE BD, ejemplo: acl_koncilia')
}

value_template_list = {
    "is_enum": "0",
    "model": "templates",
    "schema": "acl_koncilia"
}

# template_create = {
#     "model": fields.String(required=True, description='NOMBRE único del objeto'),
#     "schema": fields.String(required=True, description='NOMBRE único del objeto'),
#     "id_name": fields.String(required=True, description='NOMBRE único del objeto'),
#     "object_new": {
#         "id" : fields.Integer(readOnly=True, description='ID único del objeto'),
#         "name" : "INTERFAZ PRUEBA 11999",
#         "tc_template_id" : 1,
#         "separator" : ";",
#         "separator_line" : "...",
#         "e_state" : "Listo",
#         "header" :  [
#             {"name" : "H1", "length" : 10, "isEditable" : "true"},
#             {"name" : "H2", "length" : 15, "isEditable" : "true"},
#             {"name" : "H3", "length" : 15, "isEditable" : "true"}
#         ]
#     }
# }

value_template_create = {
    "model": "templates",
    "schema": "acl_koncilia",
    "id_name": "id",
    "object": {
        "name" : "INTERFAZ PRUEBA 11999",
        "tc_template_id" : 1,
        "separator" : ";",
        "separator_line" : "...",
        "e_state" : "Listo",
        "header" :  [
            {"name" : "H1", "length" : 10, "isEditable" : "true"},
            {"name" : "H2", "length" : 15, "isEditable" : "true"},
            {"name" : "H3", "length" : 15, "isEditable" : "true"}
        ]
    }
}

value_template_update = {
    "model": "templates",
    "schema": "acl_koncilia",
    "id_name": "id",
    "object_new": {
        "id" : 1,
        "name" : "INTERFAZ PRUEBA 11999",
        "tc_template_id" : 1,
        "separator" : ";",
        "separator_line" : "...",
        "e_state" : "Listo",
        "header" :  [
            {"name" : "H1", "length" : 10, "isEditable" : "true"},
            {"name" : "H2", "length" : 15, "isEditable" : "true"},
            {"name" : "H3", "length" : 15, "isEditable" : "true"}
        ]
    }

}

# INTERFACES

interface = {
    'id': fields.Integer(readOnly=True, description='ID único del objeto'),
    'e_origin': fields.String(required=True, description='ENUMERADOR ESTADO, referencia al enum tc_origin'),
    'user_id': fields.String(required=True, description='ID referencia al objeto/tabla users'),
    'url': fields.String(required=True, description='URL, referencia al SFTP'),
    'username': fields.String(required=True, description='USERNAME, referencia login al SFTP'),
    'password': fields.String(required=True, description='PASSWORD, referencia login al SFTP'),
    'name': fields.String(required=True, description='NOMBRE único del objeto'),
    'template_id': fields.String(required=True, description='ID referencia al objeto/tabla templates'),
    'cron_time': fields.String(required=True, description='TIEMPO DEL CRON, en formato cron time (* * * * *)'),
}

interface_list = {
    "is_enum": fields.String(required=True, description='ENUMERADOR ESTADO, toma valor 0 ó 1'),
    "model": fields.String(required=True, description='NOMBRE DEL MODELO, debe ser "interfaces"'),
    "schema": fields.String(required=True, description='NOMBRE DEL SCHEMA DE BD, ejemplo: acl_koncilia')
}

value_interface_list = {
    "is_enum": "0",
    "model": "interfaces",
    "schema": "acl_koncilia"
}

value_interface_create = {
    "model": "interfaces",
    "schema": "acl_koncilia",
    "id_name": "id",
    "object": {
        "e_origin" : "FTP",
        "user_id" : 2,
        "url" : "http://127.0.0.1",
        "username" : "koncil",
        "password" : "koncil2021",
        "name" : "INTERFACE 448966QQPP",
        "template_id" : 1,
        "cron_time" :  "* * * * *"
    }
}

value_interface_update = {
    "model": "interfaces",
    "schema": "acl_koncilia",
    "id_name": "id",
    "object_new": {
        "id" : 1,
        "e_origin" : "FTP",
        "user_id" : 2,
        "url" : "http://127.0.0.1",
        "username" : "koncil",
        "password" : "koncil2021",
        "name" : "INTERFACE 448966QQPP",
        "template_id" : 1,
        "cron_time" :  "* * * * *"
    }
}

value_tc_template_list = {
    "is_enum": "0",
    "model": "tc_templates",
    "schema": "acl_koncilia"
}

value_tc_origin_list = {
    "is_enum": "1",
    "model": "tc_origin",
    "schema": "acl_koncilia"
}

value_tc_entity_type_list = {
    "is_enum": "1",
    "model": "tc_entity_type",
    "schema": "acl_koncilia"
}

value_tc_state_list = {
    "is_enum": "1",
    "model": "tc_state",
    "schema": "acl_koncilia"
}

value_tc_file_format_list = {
    "is_enum": "1",
    "model": "tc_file_format",
    "schema": "acl_koncilia"
}

value_tc_separator_list = {
    "is_enum": "1",
    "model": "tc_separator",
    "schema": "acl_koncilia"
}

secret = 'oh_so_secret oh_my_only_secret'

def function_process_request(request, value, authorization):
    value = help_func.replacer(str(value), ["'"], ['"']).encode(encoding='UTF-8')
    request.data = value
    request.headers = dict(request.headers)
    request.headers['authorization'] = authorization