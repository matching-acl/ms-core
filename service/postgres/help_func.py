import ast
import json
import random
import string
import unicodedata


def format_valid_params_values(parameters_dict, function_parameters, list_valid_names):
    if all(item in function_parameters for item in list_valid_names):
        for key, value in parameters_dict.items():
            try:
                v = ast.literal_eval(value)
                if isinstance(v, list) or isinstance(v, dict):
                    parameters_dict[key] = v
            except Exception as e:
                parameters_dict[key] = value
        return True


def format_valid_values(parameters_dict):
    for key, value in parameters_dict.items():
        try:
            v = ast.literal_eval(value)
            if isinstance(v, list) or isinstance(v, dict):
                parameters_dict[key] = v
        except Exception as e:
            parameters_dict[key] = value
    return True


def list_from_line(line, separator, list_header):
    if separator is not None and not separator == "":
        return line.split(separator)
    else:
        list = []
        counter = 0
        for value in list_header:
            list.append(line[counter:counter + int(value['length'])])
            counter += int(value['length'])
        if line == ''.join(x for x in list):
            return list
        else:
            list.append(line[len(list) - 1:])
            return list


def parameters_to_dict(request):
    if len(str(request.data.decode('utf-8'))) > 0:
        parameters = str(request.data.decode('utf-8'))
        parameters = json.loads(parameters)
    else:
        parameters = dict(request.form)
    return parameters


def replacer(str, list_origin, list_final):
    for i in range(len(list_origin)):
        str = str.replace(list_origin[i], list_final[i])
    return str


def coder(str):
    array = strip_accents('_'.join(str.split()).lower()).split('_')
    if len(array) >= 5:
        str = ''.join(x[0] for x in array)[:5]
    else:
        diff = 5 - len(array)
        str = coder_random(str, diff)
    return str


def coder_random(str, iterations):
    array = strip_accents('_'.join(str.split()).lower()).split('_')
    str = ''.join(x[0] for x in array)
    str += random_selection(iterations)
    return str


def random_selection(iterations):
    result = ''
    for i in range(iterations):
       result += random.choice(string.ascii_letters).lower()
    return result


def strip_accents(str):
    return unicodedata.normalize('NFD', str)\
           .encode('ascii', 'ignore')\
           .decode("utf-8")


def underscore_to_camelcase(text):
    text = ''.join(map(lambda x:x, text.title().split('_')))
    return text.replace(text[0], text[0].lower())


def coder_templates(text):
    text = strip_accents(text).upper()
    text = ' '.join(text.split())
    text = text.translate({ord(c): "" for c in "!@#$%^&*()[]{};:,./<>?\|`~-=+"})
    return '_'.join(map(lambda x: x, text.split(' ')))


def convert_valid_names(text):
    text = strip_accents(text).lower()
    text = ' '.join(text.split())
    return text


# s = strip_accents('àéêöhello')
# print(s)
# print(coder('hola                     POOóéñgdfhgdfhfdh                hjhj '))

# print(coder_templates('ho.la                     POOóéñgdfhgd...fhfdh                hjhj  (werwerwer)'))
# print(coder_templates('CUENTA_CTE._   DO              CTO'))

# print(convert_valid_names('ho.la                     POOóéñgdfhgd...fhfdh                hjhj  (werwerwer)'))