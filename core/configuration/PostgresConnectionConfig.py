import os


class PostgresConnectionConfig:
    def __init__(self):
        self.__user = os.environ['POSTGRES_USER']
        self.__password = os.environ['POSTGRES_PASSWORD']
        self.__host = os.environ['POSTGRES_HOST']
        self.__port = os.environ['POSTGRES_PORT']
        self.__database = os.environ['POSTGRES_DB']

    @property
    def user(self):
        return self.__user

    @property
    def password(self):
        return self.__password

    @property
    def host(self):
        return self.__host

    @property
    def port(self):
        return self.__port

    @property
    def database(self):
        return self.__database