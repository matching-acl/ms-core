import os
from pymongo import MongoClient

from core.configuration.MongoDBConnectionConfig import MongoDBConnectionConfig


class MongoDBConnection:

    def __init__(self):
        self.DBInfo = MongoDBConnectionConfig()
        host, user, password = os.environ['MONGO_HOST'], os.environ['MONGO_USER'], os.environ['MONGO_PASS']
        uri2 = f'mongodb+srv://{user}:{password}@{host}/?ssl=true'

        self.client = MongoClient(uri2)
        # self.client = MongoClient(host=host, username=user, password=password)
        # self.client = MongoClient(host='35.188.49.207')
        # self.client = MongoClient(host=os.environ['MONGO_LOCAL_HOST'])
        # print('LOCAL HOST: ' + os.environ['MONGO_LOCAL_HOST'])

    def make_connection(self, collection=""):
        collection = collection if collection != "" else self.DBInfo.collection
        # print(list(self.client['matching']['initial'].find()))
        return self.client[collection]

    def close_connection(self):
        self.client.close()


