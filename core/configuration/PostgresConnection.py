import psycopg2
from psycopg2.extras import RealDictCursor
from psycopg2 import Error

from core.configuration.PostgresConnectionConfig import PostgresConnectionConfig


class PostgresConnection:
    def __init__(self):
        self.DB_info = PostgresConnectionConfig()
        self.connection = psycopg2.connect(
            user=self.DB_info.user,
            password=self.DB_info.password,
            host = self.DB_info.host,
            port = self.DB_info.port,
            database = self.DB_info.database
        )
        self.cursor = self.connection.cursor()

    def connectInfo(self):
        try:
            print("PostgreSQL server information")
            print(self.connection.get_dsn_parameters(), "\n")
            self.cursor.execute("SELECT version();")
            record = self.cursor.fetchone()
            print("You are connected to - ", record, "\n")
        except (Exception, Error) as error:
            print("Error while connecting to PostgreSQL", error)

    def close(self):
        self.cursor.close()
        self.connection.close()

    def set_cursor_dictionary(self):
        self.connection.cursor(cursor_factory=RealDictCursor)

    def execute(self, query, data=None):
        self.cursor.execute(query, data)

    def commit(self):
        self.connection.commit()


# PostgresConnection().connectInfo