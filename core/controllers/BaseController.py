# -- coding: UTF-8 --
import datetime
import json
import uuid


class BaseController:
    
    
    def __init__(self):
        """Initializer."""

    def serialize_json(self, obj):
        """
        Serializes objects using json.
        :param obj:
        :return:
        """
        return json.dumps(obj, default=self.serialize_datetime(obj))

    @staticmethod
    def serialize_datetime(obj):
        """
        Serializes objects if isinstance datetime
        :param obj:
        :return:
        """
        return obj.isoformat() if isinstance(obj, datetime.datetime) or isinstance(obj, datetime.date) else None

    @staticmethod
    def new_guid():
        """
         Returns an unique identifier.
        :return:
        """
        return str(uuid.uuid4())
