
class OutgoingsController():

    def __init__(self, i_outgoings_repository, i_postgres_repository):
        self.__i_postgres_repository = i_postgres_repository
        self.__i_outgoings_repository = i_outgoings_repository

    @property
    def outgoings_repository(self):
        return self.__i_outgoings_repository

    @property
    def postgres_repository(self):
        return self.__i_postgres_repository

    def put_object(self, request):
        return str(self.outgoings_repository.put_object(request, self.postgres_repository))

    def change_object(self, request):
        return str(self.outgoings_repository.change_object(request, self.postgres_repository))

    def list_objects(self, request):
        return str(self.outgoings_repository.list_objects(request, self.postgres_repository))

    def get_object(self, request):
        return str(self.outgoings_repository.get_object(request, self.postgres_repository))