
class TemplatesController():

    def __init__(self, i_templates_repository, i_postgres_repository):
        self.__i_postgres_repository = i_postgres_repository
        self.__i_templates_repository = i_templates_repository

    @property
    def templates_repository(self):
        return self.__i_templates_repository

    @property
    def postgres_repository(self):
        return self.__i_postgres_repository

    def put_object(self, request):
        return str(self.templates_repository.put_object(request, self.postgres_repository))

    def change_object(self, request):
        return str(self.templates_repository.change_object(request, self.postgres_repository))

    def list_objects(self, request):
        return str(self.templates_repository.list_objects(request, self.postgres_repository))

    def get_object(self, request):
        return str(self.templates_repository.get_object(request, self.postgres_repository))

    def download_empty_template_excel(self):
        return self.templates_repository.download_empty_template_excel()

    def template_to_excel(self, request):
        return self.templates_repository.template_to_excel(request, self.postgres_repository)

    def upload_template_excel(self, request):
        return self.templates_repository.upload_template_excel(request)