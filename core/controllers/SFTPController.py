import inject

from core.abstractions.postgres.ISFTPRepository import ISFTPRepository
from infraestructure.persistence.SFTPRepository import SFTPRepository


class SFTPController:

    def __init__(self, i_postgres_repository):
        self.__i_postgres_repository = i_postgres_repository
        self.__inject_sftp = inject.clear_and_configure(
            lambda binder: binder.bind(ISFTPRepository, SFTPRepository()))
        self.__i_sftp_repository = self.__inject_sftp.get_instance(ISFTPRepository)

    @property
    def sftp_repository(self):
        return self.__i_sftp_repository

    @property
    def postgres_repository(self):
        return self.__i_postgres_repository

    def sftp_test_connection(self, request):
        return str(self.sftp_repository.sftp_test_connection(request, self.postgres_repository))

