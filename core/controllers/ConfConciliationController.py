
class ConfConciliationController():

    def __init__(self, i_conf_conciliation_repository, i_postgres_repository, object_cron):
        self.__i_postgres_repository = i_postgres_repository
        self.__i_conf_conciliation_repository = i_conf_conciliation_repository
        self.__object_cron = object_cron

    @property
    def conf_conciliation_repository(self):
        return self.__i_conf_conciliation_repository

    @property
    def postgres_repository(self):
        return self.__i_postgres_repository

    @property
    def object_cron(self):
        return self.__object_cron

    def put_object(self, request):
        return str(self.conf_conciliation_repository.put_object(
            request, self.postgres_repository, self.object_cron
        ))

    def change_object(self, request):
        return str(self.conf_conciliation_repository.change_object(
            request, self.postgres_repository, self.object_cron
        ))

    def list_objects(self, request):
        return str(self.conf_conciliation_repository.list_objects(request, self.postgres_repository))

    def get_object(self, request):
        return str(self.conf_conciliation_repository.get_object(request, self.postgres_repository))