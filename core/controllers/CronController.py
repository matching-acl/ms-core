import inject

from core.abstractions.postgres.ICronRepository import ICronRepository
from infraestructure.persistence.CronRepository import CronRepository


class CronController:

    def __init__(self, i_cron_repository):
        self.__i_cron_repository = i_cron_repository
        self.__inject_cron = inject.clear_and_configure(lambda binder: binder.bind(ICronRepository, CronRepository()))

    @property
    def cron_repository(self):
        return self.__i_cron_repository

    def execute_cron(self, request):
        return self.cron_repository.execute_cron(request)