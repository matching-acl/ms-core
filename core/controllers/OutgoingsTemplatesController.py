from psycopg2._psycopg import InterfaceError

class OutgoingsTemplatesController():

    def __init__(self, i_outgoings_templates_repository, i_postgres_repository):
        self.__i_postgres_repository = i_postgres_repository
        self.__i_outgoings_templates_repository = i_outgoings_templates_repository

    @property
    def outgoings_templates_repository(self):
        return self.__i_outgoings_templates_repository

    @property
    def postgres_repository(self):
        return self.__i_postgres_repository

    def put_object(self, request):
        return str(self.outgoings_templates_repository.put_object(request, self.postgres_repository))

    def change_object(self, request):
        return str(self.outgoings_templates_repository.change_object(request, self.postgres_repository))

    def list_objects(self, request, count):
        # return str(self.outgoings_templates_repository.list_objects(request, self.postgres_repository))
        try:
            value = str(self.outgoings_templates_repository.list_objects(request, self.postgres_repository))
        except InterfaceError as exc:
            count += 1
            if count <= 3:
                value = self.list_objects(request, count)
        return value

    def get_object(self, request):
        return str(self.outgoings_templates_repository.get_object(request, self.postgres_repository))