import json
import inject

from core.abstractions.postgres.IConfConciliationRepository import IConfConciliationRepository
from core.abstractions.postgres.ICronRepository import ICronRepository
from core.abstractions.postgres.IOutgoingsRepository import IOutgoingsRepository
from core.abstractions.postgres.IOutgoingsTemplatesRepository import IOutgoingsTemplatesRepository
from core.abstractions.postgres.ITemplatesRepository import ITemplatesRepository
from core.abstractions.postgres.IInterfacesRepository import IInterfacesRepository
from core.controllers.ConfConciliationController import ConfConciliationController
from core.controllers.CronController import CronController
from core.controllers.OutgoingsController import OutgoingsController
from core.controllers.OutgoingsTemplatesController import OutgoingsTemplatesController
from core.controllers.TemplatesController import TemplatesController
from core.controllers.InterfacesController import InterfacesController
from infraestructure.persistence.ConfConciliationRepository import ConfConciliationRepository
from infraestructure.persistence.CronRepository import CronRepository
from infraestructure.persistence.OutgoingsRepository import OutgoingsRepository
from infraestructure.persistence.OutgoingsTemplatesRepository import OutgoingsTemplatesRepository
from infraestructure.persistence.TemplatesRepository import TemplatesRepository
from infraestructure.persistence.InterfacesRepository import InterfacesRepository
from infraestructure.resources.message_response import MessageResponse
from infraestructure.resources.model_resource import ModelResource
from infraestructure.resources.params_resource import ParamsResource
from infraestructure.resources.status_response import StatusResponse
from service.postgres import help_func

class PostgresController:

    def __init__(self, i_postgres_repository, object_cron):
        self.__i_postgres_repository = i_postgres_repository
        self.__object_cron = object_cron
        self.__inject_templates = inject.clear_and_configure(
            lambda binder: binder.bind(ITemplatesRepository, TemplatesRepository())
        )
        self.__templates_controller = TemplatesController(
            self.__inject_templates.get_instance(ITemplatesRepository), i_postgres_repository
        )
        self.__inject_cron = inject.clear_and_configure(
            lambda binder: binder.bind(ICronRepository, CronRepository())
        )
        self.__cron_controller = CronController(
            self.__inject_cron.get_instance(ICronRepository)
        )
        self.__inject_interfaces = inject.clear_and_configure(
            lambda binder: binder.bind(IInterfacesRepository, InterfacesRepository())
        )
        self.__interfaces_controller = InterfacesController(
            self.__inject_interfaces.get_instance(IInterfacesRepository),
            i_postgres_repository,
            object_cron
        )
        self.__inject_conf_conciliation = inject.clear_and_configure(
            lambda binder: binder.bind(IConfConciliationRepository, ConfConciliationRepository())
        )
        self.__conf_conciliation_controller = ConfConciliationController(
            self.__inject_conf_conciliation.get_instance(IConfConciliationRepository),
            i_postgres_repository,
            object_cron
        )
        self.__inject_outgoings = inject.clear_and_configure(
            lambda binder: binder.bind(IOutgoingsRepository, OutgoingsRepository(object_cron))
        )
        self.__outgoings_controller = OutgoingsController(
            self.__inject_outgoings.get_instance(IOutgoingsRepository), i_postgres_repository
        )
        self.__inject_outgoings_templates = inject.clear_and_configure(
            lambda binder: binder.bind(IOutgoingsTemplatesRepository, OutgoingsTemplatesRepository())
        )
        self.__outgoings_templates_controller = OutgoingsTemplatesController(
            self.__inject_outgoings_templates.get_instance(IOutgoingsTemplatesRepository), i_postgres_repository
        )
        self.__dict_of_controllers = {
            'templates' : self.__templates_controller,
            'interfaces': self.__interfaces_controller,
            'cron' : self.__cron_controller,
            'conf_conciliation': self.__conf_conciliation_controller,
            'outgoings': self.__outgoings_controller,
            'outgoings_templates': self.__outgoings_templates_controller
        }
        self.params = ParamsResource()
        self.message_class = MessageResponse()
        self.status_class = StatusResponse()
        self.models = ModelResource()

    @property
    def postgres_repository(self):
        return self.__i_postgres_repository

    @property
    def object_cron(self):
        return self.__object_cron

    def put_object(self, request):
        parameters = help_func.parameters_to_dict(request)
        if self.params.model in parameters:
            if parameters[self.params.model] in self.__dict_of_controllers:
                return self.__dict_of_controllers[parameters[self.params.model]].put_object(request)
        result = self.message_class.message_invalids_params + self.message_class.message_extra_model
        status = self.status_class.status_error
        return json.dumps(
            {self.params.status: status, self.params.message: result},
            ensure_ascii=False
        ).encode(self.params.utf8).decode()

    def change_object(self, request):
        parameters = help_func.parameters_to_dict(request)
        if self.params.model in parameters:
            if parameters[self.params.model] in self.__dict_of_controllers:
                return self.__dict_of_controllers[parameters[self.params.model]].change_object(request)
        result = self.message_class.message_invalids_params + self.message_class.message_extra_model
        status = self.status_class.status_error
        return json.dumps(
            {self.params.status: status, self.params.message: result},
            ensure_ascii=False
        ).encode(self.params.utf8).decode()

    def get_object(self, request):
        parameters = help_func.parameters_to_dict(request)
        if self.params.model in parameters:
            return self.__dict_of_controllers[parameters[self.params.model]].get_object(request)
        return self.postgres_repository.get_object(request)

    def list_objects(self, request):
        parameters = help_func.parameters_to_dict(request)
        if parameters.get(self.params.model, None) in self.__dict_of_controllers:
            if parameters.get(self.params.model, None) != 'outgoings_templates':
                return self.__dict_of_controllers[parameters.get(self.params.model, None)].list_objects(request)
            else:
                count = 0
                return self.__dict_of_controllers[parameters.get(self.params.model, None)].list_objects(request, count)
        return self.postgres_repository.list_objects(request)

    def remove_object(self, schema="", model="", id_name="", id=None):
        return self.postgres_repository.remove_object(schema, model, id_name, id)

    def total_items(self, request):
        return self.postgres_repository.total_items(request)

    def execute_cron(self, request):
        return self.__cron_controller.execute_cron(request)

    def cron_time_text(self, request):
        return self.postgres_repository.json_cron_description(request)

    def shutdown_cron(self):
        return self.postgres_repository.shutdown_cron(self.object_cron)

    def download_empty_template_excel(self):
        return self.__dict_of_controllers[self.models.model_templates].download_empty_template_excel()

    def template_to_excel(self, request):
        return self.__dict_of_controllers[self.models.model_templates].template_to_excel(request)

    def upload_template_excel(self, request):
        return self.__dict_of_controllers[self.models.model_templates].upload_template_excel(request)



