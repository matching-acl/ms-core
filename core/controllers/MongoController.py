# -- coding: UTF-8 --
from core.controllers.BaseController import BaseController


class MongoController(BaseController):
    """
    Class DemoController
    """

    def __init__(self, i_mongo_repository):
        """Initializer."""
        self.iMongoRepository = i_mongo_repository

    def get_object(self):
        return self.iMongoRepository.get_object()

    def run_conciliation(self, payload):
        print(payload)
        reconciliations = payload['conciliation'] if 'conciliation' in payload else []

        if len(reconciliations) < 2:
            raise Exception('The reconciliation process must instantiate two or more interfaces.')

        self.iMongoRepository.run_conciliation(reconciliations)

