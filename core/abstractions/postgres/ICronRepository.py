from interface import Interface
from datetime import timedelta


class ICronRepository(Interface):

    def add_job(self, model, object):
        pass

    def execute_cron(self, model, object):
        pass

    def stop_cron(self, id_job):
        pass

    def round_down_time(self, dt=None, dateDelta=timedelta(minutes=1)):
        pass

    def get_next_cron_run_time(self, schedule):
        pass

    def sleep_till_top_of_next_minute(self):
        pass