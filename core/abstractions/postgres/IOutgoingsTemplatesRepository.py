from interface import Interface

class IOutgoingsTemplatesRepository(Interface):

    def make_connection(self):
        pass

    def close_connection(self):
        pass

    def put_object(self, request, i_postgres_repository):
        pass

    def change_object(self, request, i_postgres_repository):
        pass

    def list_objects(self, request, i_postgres_repository):
        pass

    def get_object(self, request, i_postgres_repository):
        pass

    def query_selectors_read(self, schema):
        pass

    def query_joins_read(self, schema):
        pass

    def validate_inactivation(self, parameters, i_postgres_repository):
        pass

    def validate_activation(self, parameters, i_postgres_repository):
        pass

    def rollback_error(self):
        pass
