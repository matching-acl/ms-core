from interface import Interface


class ISFTPRepository(Interface):

    def connection(self, username, password, url, port, e_origin):
        pass

    def is_connected(self, username, password, url, port, e_origin):
        pass

    def sftp_test_connection(self, request, i_postgres_repository):
        pass