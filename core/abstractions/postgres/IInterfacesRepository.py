from interface import Interface

class IInterfacesRepository(Interface):

    def make_connection(self):
        pass

    def close_connection(self):
        pass

    def put_object(self, request, i_postgres_repository, object_cron):
        pass

    def change_object(self, request, i_postgres_repository, object_cron):
        pass

    def list_objects(self, request, i_postgres_repository):
        pass

    def get_object(self, request, i_postgres_repository):
        pass

    def query_selectors_read(self, schema):
        pass

    def query_joins_read(self, schema):
        pass

    def validate_activation(self, parameters, i_postgres_repository):
        pass

    def validate_inactivation(self, parameters, i_postgres_repository):
        pass

    def rollback_error(self):
        pass

    # def get_template(self):
    #     pass
    #
    # def request_for_load_data(self):
    #     pass
    #
    # def copy_files_job_interfaces(self):
    #     pass
    #
    # def process_job_interfaces(self):
    #     pass