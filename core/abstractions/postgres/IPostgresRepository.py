from interface import Interface

class IPostgresRepository(Interface):

    def make_connection(self):
        pass

    def close_connection(self):
        pass

    def get_object(self, request):
        pass

    def list_objects(self, request):
        pass

    def find_object(self, schema="", model="", filters=None, attrs=None):
        pass

    def find_all(self, schema="", model="", filters=None, attrs=None):
        pass

    def total_items(self, request):
        pass

    def change_object(self, schema="", model="", id_name="", object=None):
        pass

    def remove_object(self, schema="", model="", id_name="", id=None):
        pass

    def available_code(self, name, schema, model):
        pass

    def execute(self, connection, cursor, query, object):
        pass

    def rollback_error(self):
        pass

    def my_list_converter(self, object):
        pass

    def my_standard_converter(self, object):
        pass

    def perfect_eval(self, anonstring):
        pass

    def result_to_json(self, status, message):
        pass

    def result_to_extra_json(self, dictionary):
        pass

    def cron_description(self, cron_time):
        pass

    def json_cron_description(self, request):
        pass
