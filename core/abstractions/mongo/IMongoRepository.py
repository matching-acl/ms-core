from interface import Interface


class IMongoRepository(Interface):

    def run_conciliation(self, collections):
        pass

    def make_connection(self, collection=""):
        pass

    def put_object(self):
        pass

    def get_object(self, document="", filters=""):
        pass

    def get_all_object(self, document=""):
        pass

    def list_objects(self):
        pass

    def remove_object(self):
        pass
